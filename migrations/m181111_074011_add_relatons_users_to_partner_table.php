<?php

use yii\db\Migration;

/**
 * Class m181111_074011_add_relatons_users_to_partner_table
 */
class m181111_074011_add_relatons_users_to_partner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'partner_id', $this->integer()->comment('Код партнера'));

        // creates index for column `partner_id`
        $this->createIndex(
            'idx-users-partner_id',
            'users',
            'partner_id'
        );

        // add foreign key for table `partner`
        $this->addForeignKey(
            'fk-users-partner_id',
            'users',
            'partner_id',
            'partner',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181111_074011_add_relatons_users_to_partner_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181111_074011_add_relatons_users_to_partner_table cannot be reverted.\n";

        return false;
    }
    */
}
