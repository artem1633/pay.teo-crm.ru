<?php

use yii\db\Migration;

/**
 * Class m181107_184031_add_sms_gate_setting_to_settings_table
 */
class m181107_184031_add_sms_gate_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'Логин СМС шлюза',
            'key' => 'sms_gate_login',
        ]);

        $this->insert('settings', [
            'name' => 'Пароль СМС шлюза',
            'key' => 'sms_gate_password',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181107_184031_add_sms_gate_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181107_184031_add_sms_gate_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
