<?php

use yii\db\Migration;

/**
 * Handles adding offers to table `driver`.
 */
class m181127_161753_add_offers_columns_to_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('driver', 'accept_offer', $this->smallInteger(1)->comment('Согласие с офертой'));
        $this->addColumn('driver', 'link_offer', $this->text()->comment('Ссылка на оферту'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_161753_add_offers_columns_to_driver_table cannot be reverted.\n";

        return false;
    }
}
