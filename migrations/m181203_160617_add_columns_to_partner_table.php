<?php

use yii\db\Migration;

/**
 * Class m181203_160617_add_columns_to_partner_table
 */
class m181203_160617_add_columns_to_partner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('partner', 'min_city_balance', $this->integer(13)->comment('Минимальный баланс в Ситимобил'));
        $this->addColumn('partner', 'min_qiwi_balance', $this->integer(13)->comment('Минимальный баланс в QIWI'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181203_160617_add_columns_to_partner_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181203_160617_add_columns_to_partner_table cannot be reverted.\n";

        return false;
    }
    */
}
