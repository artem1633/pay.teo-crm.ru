<?php

use yii\db\Migration;

/**
 * Class m181126_174438_add_type_payment_table
 */
class m181126_174438_add_type_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('type_payment', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->insert('type_payment', [
           'name' => 'QIWI',
        ]);
        $this->insert('type_payment', [
            'name' => 'Карта',
        ]);

        $this->addColumn('driver', 'type_payment_id', $this->integer()->comment('Код типа платежа'));
        $this->addColumn('driver', 'card_number', $this->integer(20)->comment('Номер карты'));

        // creates index for column `type_payment_id`
        $this->createIndex(
            'idx-driver-type_payment_id',
            'driver',
            'type_payment_id'
        );

        // add foreign key for table `type_payment`
        $this->addForeignKey(
            'fk-driver-type_payment_id',
            'driver',
            'type_payment_id',
            'type_payment',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('type_payment');
       $this->dropColumn('driver', 'type_payment_id');
       $this->dropColumn('driver', 'card_number');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_174438_add_type_payment_table cannot be reverted.\n";

        return false;
    }
    */
}
