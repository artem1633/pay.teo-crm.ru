<?php

use yii\db\Migration;

/**
 * Handles the creation of table `import_error`.
 */
class m181219_150812_create_import_error_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('import_error', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->comment('Дата создания записи'),
            'user_id' => $this->integer()->comment('ID пользователя, выполняющего импорт'),
            'user_ip' => $this->string()->comment('IP адрес пользователя, выполняющего импорт'),
            'description' => $this->text()->comment('Описание ошибки'),
            'driver_id' => $this->integer()->comment('ID водителя (получателя выплаты)'),
            'amount' => $this->double(2)->comment('Сумма выплаты'),
            'type_payment' => $this->integer()->comment('Тип выплаты'),
        ]);

        // creates index for column `driver_id`
        $this->createIndex(
            'idx-import_error-driver_id',
            'import_error',
            'driver_id'
        );

        // add foreign key for table `import_error`
        $this->addForeignKey(
            'fk-import_error-driver_id',
            'import_error',
            'driver_id',
            'driver',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-import_error-user_id',
            'import_error',
            'user_id'
        );

        // add foreign key for table `partner`
        $this->addForeignKey(
            'fk-error_import-user_id',
            'import_error',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-import_error-type_payment',
            'import_error',
            'type_payment'
        );

        // add foreign key for table `partner`
        $this->addForeignKey(
            'fk-error_import-type_payment',
            'import_error',
            'type_payment',
            'type_payment',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('import_error');
    }
}
