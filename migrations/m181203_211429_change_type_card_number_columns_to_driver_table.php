<?php

use yii\db\Migration;

/**
 * Class m181203_211429_change_type_card_number_columns_to_driver_table
 */
class m181203_211429_change_type_card_number_columns_to_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('driver', 'card_number', $this->string()->comment('Номер карты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181203_211429_change_type_card_number_columns_to_driver_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181203_211429_change_type_card_number_columns_to_driver_table cannot be reverted.\n";

        return false;
    }
    */
}
