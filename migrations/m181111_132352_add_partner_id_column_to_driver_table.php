<?php

use yii\db\Migration;

/**
 * Handles adding partner_id to table `driver`.
 */
class m181111_132352_add_partner_id_column_to_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('driver', 'partner_id', $this->integer()->comment('Код партнера'));

        // creates index for column `partner_id`
        $this->createIndex(
            'idx-driver-partner_id',
            'driver',
            'partner_id'
        );

        // add foreign key for table `partner`
        $this->addForeignKey(
            'fk-driver-partner_id',
            'driver',
            'partner_id',
            'partner',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181111_132352_add_partner_id_column_to_driver_table cannot be reverted.\n";

        return false;
    }
}
