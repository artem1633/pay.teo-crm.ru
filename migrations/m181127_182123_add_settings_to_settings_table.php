<?php

use yii\db\Migration;

/**
 * Class m181127_182123_add_settings_to_settings_table
 */
class m181127_182123_add_settings_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'СМС тект водителю (при регистрации)',
            'key' => 'driver_sms_text',
            'value' => '{фио_водителя}! Вы зарегистрированы в системе PayPartner. Пройдите по ссылке и примите условия оферты. Ссылка: {ссыслка}'
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_182123_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181127_182123_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
