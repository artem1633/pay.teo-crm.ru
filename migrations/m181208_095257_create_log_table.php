<?php

use yii\db\Migration;

/**
 * Handles the creation of table `log`.
 */
class m181208_095257_create_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('log', [
            'level' =>$this->integer()->comment('Уровень записи лога'),
            'category' => $this->string()->comment('Категория записи лога'),
            'log_time' => $this->integer()->comment('Время записи'),
            'prefix' => $this->string(),
            'message' => $this->text()->comment('Сообщение'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('log');
    }
}
