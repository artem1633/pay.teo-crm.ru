<?php

use yii\db\Migration;

/**
 * Class m181129_090718_change_type_status_column_in_city_payment_table
 */
class m181129_090718_change_type_status_column_in_city_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('city_payment', 'status', $this->integer(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181129_090718_change_type_status_column_in_city_payment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181129_090718_change_type_status_column_in_city_payment_table cannot be reverted.\n";

        return false;
    }
    */
}
