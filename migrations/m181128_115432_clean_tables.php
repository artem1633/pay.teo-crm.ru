<?php

use yii\db\Migration;

/**
 * Class m181128_115432_clean_tables
 */
class m181128_115432_clean_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('partner_user');
        $this->dropColumn('users', 'name');
        $this->dropColumn('users', 'middle_name');
        $this->dropColumn('users', 'accept_offer');
        $this->dropColumn('users', 'link_offer');
        $this->dropColumn('users', 'text_offer');
        $this->dropColumn('users', 'balance');
        $this->dropColumn('users', 'party_system_id');
        $this->dropColumn('users', 'qiwi');
        $this->dropColumn('users', 'payment_account');
        $this->dropColumn('users', 'bik_bank');
        $this->dropColumn('users', 'recived_money');
        $this->dropColumn('users', 'telegram_nickname');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181128_115432_clean_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181128_115432_clean_tables cannot be reverted.\n";

        return false;
    }
    */
}
