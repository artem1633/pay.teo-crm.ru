<?php

use yii\db\Migration;

/**
 * Class m181130_073340_add_default_values_columns_to_tables
 */
class m181130_073340_add_default_values_columns_to_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('partner', 'percent', $this->double()->defaultValue(0));
        $this->alterColumn('partner', 'time_to_payment', $this->integer()->defaultValue(0));
        $this->alterColumn('partner', 'month_limit', $this->integer()->defaultValue(0));
        $this->alterColumn('partner', 'day_limit', $this->integer()->defaultValue(0));
        $this->alterColumn('partner', 'transaction_limit', $this->integer()->defaultValue(0));
        $this->alterColumn('partner', 'access_to_payments', $this->integer()->defaultValue(0));
        $this->alterColumn('partner', 'balance', $this->integer()->defaultValue(0));
        $this->alterColumn('partner', 'waiting_recharge', $this->integer()->defaultValue(0));

        $this->alterColumn('payment', 'amount', $this->integer()->defaultValue(0));

        $this->alterColumn('city_payment', 'amount', $this->integer()->defaultValue(0));
        $this->alterColumn('city_payment', 'status', $this->integer()->defaultValue(1));
        $this->alterColumn('city_payment', 'remains', $this->integer()->defaultValue(0));
        $this->alterColumn('city_payment', 'percent_retention', $this->double()->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181130_073340_add_default_values_columns_to_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181130_073340_add_default_values_columns_to_tables cannot be reverted.\n";

        return false;
    }
    */
}
