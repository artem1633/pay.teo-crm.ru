<?php

use yii\db\Migration;

/**
 * Handles adding read_only to table `users`.
 */
class m181207_184553_add_read_only_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'read_only', $this->smallInteger(1)
            ->comment('Только чтение')
            ->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181207_184553_add_read_only_column_to_users_table cannot be reverted.\n";

        return false;
    }
}
