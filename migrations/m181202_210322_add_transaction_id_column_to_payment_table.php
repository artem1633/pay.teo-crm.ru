<?php

use yii\db\Migration;

/**
 * Handles adding transaction_id to table `payment`.
 */
class m181202_210322_add_transaction_id_column_to_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment', 'transaction_id', $this->string(20)->comment('ID транзакции в системе QIWI'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181202_210322_add_transaction_id_column_to_payment_table cannot be reverted.\n";

        return false;
    }
}
