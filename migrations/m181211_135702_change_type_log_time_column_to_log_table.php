<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m181211_135702_change_type_log_time_column_to_log_table
 */
class m181211_135702_change_type_log_time_column_to_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('log');

        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'date' => $this->timestamp()->comment('Дата записи')->defaultValue(new Expression('NOW()')),
            'partner_id' => $this->integer(),
            'user_id' => $this->integer(),
            'user_ip' => $this->string(),
            'description' => $this->string(),
            'result' => $this->string(),
            'amount' => $this->double(2),
            'payee' => $this->string(),

        ]);

        $this->alterColumn('city_payment', 'payment_datetime', $this->timestamp()->defaultValue(new Expression('NOW()')));
        $this->alterColumn('payment', 'create_datetime', $this->timestamp()->defaultValue(new Expression('NOW()')));
        $this->dropColumn('payment', 'payment_datetime');
        $this->addColumn('payment', 'payment_datetime', $this->timestamp()->defaultValue(new Expression('NOW()')));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('log', 'date', 'log_time');
//        echo "m181211_135702_change_type_log_time_column_to_log_table cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_135702_change_type_log_time_column_to_log_table cannot be reverted.\n";

        return false;
    }
    */
}
