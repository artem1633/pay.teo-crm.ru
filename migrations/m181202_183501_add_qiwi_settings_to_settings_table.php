<?php

use yii\db\Migration;

/**
 * Class m181202_183501_add_qiwi_settings_to_settings_table
 */
class m181202_183501_add_qiwi_settings_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'QIWI токен',
            'key' => 'qiwi_token',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181202_183501_add_qiwi_settings_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181202_183501_add_qiwi_settings_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
