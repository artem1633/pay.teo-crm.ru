<?php

use yii\db\Migration;

/**
 * Class m181209_155432_change_type_column_to_city_payment_table
 */
class m181209_155432_change_type_column_to_city_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('city_payment', 'amount', $this->double(2)->comment('Сумма зачисления'));
        $this->alterColumn('city_payment', 'remains', $this->double(2)->comment('Баланс на момент зачисления'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181209_155432_change_type_column_to_city_payment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181209_155432_change_type_column_to_city_payment_table cannot be reverted.\n";

        return false;
    }
    */
}
