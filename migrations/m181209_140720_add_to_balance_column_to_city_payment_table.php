<?php

use yii\db\Migration;

/**
 * Handles adding to_balance to table `city_payment`.
 */
class m181209_140720_add_to_balance_column_to_city_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('city_payment', 'to_balance',
            $this->timestamp()
            ->defaultValue(null)
            ->comment('Дата и время перевода суммы в основной баланс'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
