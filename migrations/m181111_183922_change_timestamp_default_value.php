<?php

use yii\db\Migration;

/**
 * Class m181111_183922_change_timestamp_default_value
 */
class m181111_183922_change_timestamp_default_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('city_payment', 'payment_datetime', $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')));
        $this->alterColumn('payment', 'create_datetime', $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')));
        $this->alterColumn('payment', 'payment_datetime', $this->timestamp()->defaultValue(null));
        $this->alterColumn('tmp', 'code_create_at', $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')));
        $this->alterColumn('tmp', 'date_end_user_ban', $this->timestamp()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181111_183922_change_timestamp_default_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181111_183922_change_timestamp_default_value cannot be reverted.\n";

        return false;
    }
    */
}
