<?php

use yii\db\Migration;

/**
 * Class m181211_101604_change_column_to_log_table
 */
class m181211_101604_change_column_to_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('log', 'id', $this->primaryKey());
        $this->addColumn('log', 'partner_id', $this->integer());
        $this->addColumn('log', 'user_id', $this->integer());
        $this->addColumn('log', 'user_ip', $this->string());
        $this->addColumn('log', 'description', $this->string());
        $this->addColumn('log', 'result', $this->string());
        $this->addColumn('log', 'amount', $this->double()->comment('Сумма'));
        $this->addColumn('log', 'payee', $this->string()->comment('Получатель платежа'));

        $this->dropColumn('log', 'level');
        $this->dropColumn('log', 'category');
        $this->dropColumn('log', 'prefix');
        $this->dropColumn('log', 'message');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181211_101604_change_column_to_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_101604_change_column_to_log_table cannot be reverted.\n";

        return false;
    }
    */
}
