<?php

use yii\db\Migration;

/**
 * Class m181214_092549_change_type_card_number_column_to_payment_table
 */
class m181214_092549_change_type_card_number_column_to_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->alterColumn('payment', 'card_number', $this->string()->comment('Номер карты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181214_092549_change_type_card_number_column_to_payment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_092549_change_type_card_number_column_to_payment_table cannot be reverted.\n";

        return false;
    }
    */
}
