<?php

use yii\db\Migration;

/**
 * Class m181203_173613_change_type_qiwi_number_column_to_payment_table
 */
class m181203_173613_change_type_qiwi_number_column_to_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payment', 'qiwi_number', $this->string());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181203_173613_change_type_qiwi_number_column_to_payment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181203_173613_change_type_qiwi_number_column_to_payment_table cannot be reverted.\n";

        return false;
    }
    */
}
