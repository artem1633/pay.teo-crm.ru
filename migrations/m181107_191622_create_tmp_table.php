<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tmp`.
 */
class m181107_191622_create_tmp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $this->createTable('tmp', [
            'id' => $this->primaryKey(),
            'code' => $this->integer()->Null()->comment('Код аутентификации'),
            'code_create_at' => $this->timestamp()->defaultValue(date('Y-m-d H:i:s'), time())->comment('Время генерации кода'),
            'user_id' => $this->integer()->notNull()->comment('Код пользоавтеля (владелец кода аутентификации)'),
            'date_end_user_ban' => $this->timestamp()->defaultValue(date('Y-m-d H:i:s'), time())->comment('Дата окончания бана пользователя за множественный неверный ввод кода пользователя'),
            'num_wrong_code' => $this->integer()->comment('Количество неудачных попыток ввода кода атентификации'),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tmp');
    }
}
