<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city_payment`.
 */
class m181106_181058_create_city_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('city_payment', [
            'id' => $this->primaryKey(),
            'payment_datetime' => $this->timestamp()->comment('Дата и время')->defaultValue(date('Y-m-d H:i:s'), time()),
            'amount' => $this->integer()->comment('Сумма'),
            'status' => $this->string()->comment('Статус - Резерв/Зачислено'),
            'owner_receipt' => $this->string()->comment('Плательщик'),
            'remains' => $this->integer()->comment('Остаток на момент зачисления'),
            'percent_retention' => $this->double()->comment('Процент списания')
        ]);

        $this->addCommentOnTable('city_payment', 'Информация по оплате из CityMobil');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('city_payment');
    }
}
