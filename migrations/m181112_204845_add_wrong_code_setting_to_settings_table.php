<?php

use yii\db\Migration;

/**
 * Class m181112_204845_add_wrong_code_setting_to_settings_table
 */
class m181112_204845_add_wrong_code_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'Количество попыток ввода СМС кода',
            'key' => 'num_wrong_code',
            'value' => '4',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181112_204845_add_wrong_code_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181112_204845_add_wrong_code_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
