<?php

use yii\db\Migration;

/**
 * Class m181109_124655_add_columns_to_users_table
 */
class m181109_124655_add_columns_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'accept_offer', $this->smallInteger(1)->comment('Согласие с офертой'));
        $this->addColumn('users', 'link_offer', $this->text()->comment('Ссылка на оферту'));
        $this->addColumn('users', 'text_offer', $this->text()->comment('Текст оферты'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181109_124655_add_columns_to_users_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181109_124655_add_columns_to_users_table cannot be reverted.\n";

        return false;
    }
    */
}
