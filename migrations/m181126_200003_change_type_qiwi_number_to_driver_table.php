<?php

use yii\db\Migration;

/**
 * Class m181126_200003_change_type_qiwi_number_to_driver_table
 */
class m181126_200003_change_type_qiwi_number_to_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('driver', 'qiwi_number', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_200003_change_type_qiwi_number_to_driver_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_200003_change_type_qiwi_number_to_driver_table cannot be reverted.\n";

        return false;
    }
    */
}
