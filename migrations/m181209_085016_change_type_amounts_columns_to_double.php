<?php

use yii\db\Migration;

/**
 * Class m181209_085016_change_type_amounts_columns_to_double
 */
class m181209_085016_change_type_amounts_columns_to_double extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('partner', 'balance', $this->double(2)->comment('Баланс'));
        $this->alterColumn('partner', 'month_limit', $this->double(2)->comment('Месячный лимит'));
        $this->alterColumn('partner', 'day_limit', $this->double(2)->comment('Дневной лимит'));
        $this->alterColumn('partner', 'transaction_limit', $this->double(2)->comment('Лимит транзакции'));
        $this->alterColumn('partner', 'min_city_balance', $this->double(2)->comment('Минимальный баланс в Ситимобмл'));
        $this->alterColumn('partner', 'min_qiwi_balance', $this->double(2)->comment('Минимальный баланс в QIWI'));

        $this->alterColumn('payment', 'amount', $this->double(2)->comment('Сумма выплаты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181209_085016_change_type_amounts_columns_to_double cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181209_085016_change_type_amounts_columns_to_double cannot be reverted.\n";

        return false;
    }
    */
}
