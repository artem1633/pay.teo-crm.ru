<?php

use yii\db\Migration;

/**
 * Handles adding card_number to table `payment`.
 */
class m181126_190110_add_card_number_column_to_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment', 'card_number', $this->integer(20)->comment('Номер карты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payment', 'card_number');
    }
}
