<?php

namespace app\modules\api\controllers;

use app\models\Settings;
use Yii;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\Response;

//use yii\data\ActiveDataProvider;
//use yii\web\NotFoundHttpException;
//use yii\web\Controller;


//use app\models\Clients;

/**
 * Default controller for the `api` module
 */
class ClientController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * Отправка в телеграм канал
     *
     * @param $text
     * @return mixed
     */
    public static function actionPushtelegram($text, $is_activation = false)
    {
        if (!$is_activation) {
            $text = urlencode($text);
        }
//        $user_id = '@sityguru';
        $user_id = Settings::getSettingValueByKey('group_name');
        $proxy_ip = Settings::getSettingValueByKey('ip_proxy');
        $proxy_port = Settings::getSettingValueByKey('port_proxy');
        $proxy = $proxy_ip . ":" . $proxy_port;
        $token = Settings::getSettingValueByKey('telegram_api_key');
        $url = 'https://api.telegram.org/bot' . $token . '/sendMessage?chat_id=' . $user_id . '&disable_web_page_preview=true&text=' . $text;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        Yii::info('Прокси: ' . $proxy, __METHOD__);
        Yii::info('URL: ' . $url, __METHOD__);
        Yii::info($curl_scraped_page, __METHOD__);

//        VarDumper::dump($curl_scraped_page, 10, true);

        return $curl_scraped_page;
    }

    public function actionSendReport()
    {
        $model = Settings::find()->where(['key' => 'report_status'])->one();
        $report_status = Settings::getSettingValueByKey('report_status'); //Статус отправки отчета
        $report_time = strtotime(date('Y-m-d ') . Settings::getSettingValueByKey('report_time') . ':00'); //Дата и Время отчета
//        $current_time = strtotime(date('Y-m-d H:i:s')); //Текущее время
        $current_time = time(); //Текущее время

        if ($current_time < $report_time && $report_status == 'true') { //Если текущее время меньше отчетного и статус отправки отчета = отправлен (true)
            $model->value = 'false'; //Статус отправки отчета
            $model->save();
        } elseif ($current_time > $report_time && $report_status == 'false') { //Если текущее время больше отчетного и статус отправки = не отправлен (false)
            $report = 'Какой-то отчет'; //Change::getReport(); //Создаем отчет
            Yii::info('Отчет: ' . $report, __METHOD__);
            self::actionPushtelegram($report); //Отправляем
            $model->value = 'true'; //Меняем статус отправки
            $model->save();
        }

        Yii::info(Date(DATE_ATOM, $report_time), __METHOD__);
        Yii::info(Date(DATE_ATOM, $current_time), __METHOD__);


        return 'ok';
    }

    /**
     * Отправка запроса в Ситимобил
     *
     * @param $method
     * @param $params
     * @param string $send_method
     * @return bool|mixed|string
     */
    public static function requestAPI($method, $params, $send_method = 'POST')
    {
        $version = '1.0.0';
        $auth_key = Settings::getSettingValueByKey('api_key_city_mobile');
        $url = "https://partner-api.city-mobil.ru/{$version}/{$method}";

        if ($send_method == 'GET') {
            $url .= "?auth_key={$auth_key}";
            //Собираем ссылку из параметров
            foreach ($params as $key => $value) {
                $url .= '&' . $key . '=' . $value;
            }
        } else {
            $params['auth_key'] = $auth_key;
        }


        Yii::info($url, __METHOD__);


        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'request_fulluri' => true,
                'method' => $send_method,
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));


        $result = Json::decode($result);

        Yii::info($result, __METHOD__);

        return $result;
    }

    /**
     * Отправка СМС сообщения
     *
     * @param string $phone_number Номер телефона в формате +79999999999
     * @param string $message Текст сообщения
     * @return bool|string
     */
    public static function requestSMS($phone_number, $message)
    {
        $url = 'http://api.iqsms.ru/messages/v2/send/';

        $params['login'] = Settings::getSettingValueByKey('sms_gate_login');
        $params['password'] = Settings::getSettingValueByKey('sms_gate_password');
        $params['phone'] = $phone_number;
        $params['text'] = $message;
        $params['flash'] = 1;

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        Yii::info($result);

        return $result;

    }


}
