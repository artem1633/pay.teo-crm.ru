<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "log".
 *
 * @property string $date Дата записи
 * @property int $id
 * @property int $partner_id
 * @property int $user_id
 * @property string $user_ip
 * @property string $description
 * @property string $result
 * @property double $amount Сумма
 * @property string $payee Получатель платежа
 */
class Log extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['partner_id', 'user_id', 'payee'], 'integer'],
            [['amount'], 'number'],
            [['date'], 'string', 'max' => 50],
            [['user_ip', 'description', 'result'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
            'id' => 'ID',
            'partner_id' => 'Код партнера',
            'user_id' => 'Код пользователя',
            'user_ip' => 'IP пользователя',
            'description' => 'Описание операции',
            'result' => 'Результат',
            'amount' => 'Сумма',
            'payee' => 'Получать платежа',
        ];
    }

}
