<?php

namespace app\models;


use app\modules\api\controllers\ClientController;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property string $create_datetime Дата и время создания выплаты
 * @property string $driver_id Код водителя
 * @property int $amount Сумма выплаты
 * @property string $status Статус
 * @property int $owner_payment_id Создатель выплаты
 * @property string $payment_datetime Дата и время выплаты
 * @property int $qiwi_number Номер QIWI
 * @property string $qiwi_answer Ответ от киви
 * @property string $card_number Номер карты
 * @property string $transaction_id ID транзакции в сервисе QIWI
 */
class Payment extends ActiveRecord
{
//    public $errors = false;

//    const PAYMENT_QIWI = 1;
//    const PAYMENT_CARD = 2;
    public $type_payment = 0; //Тип выплаты (1 - QIWI, 2 - Карта)

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qiwi_number', 'create_datetime', 'payment_datetime'], 'safe'],
            [['driver_id', 'type_payment', 'owner_payment_id'], 'integer'],
            [['card_number', 'status', 'qiwi_answer'], 'string', 'max' => 255],
            [['transaction_id'], 'string', 'max' => 20],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_datetime' => 'Дата и время создания выплаты',
            'driver_id' => 'Код водителя',
            'amount' => 'Сумма выплаты',
            'status' => 'Статус',
            'owner_payment_id' => 'Создатель выплаты',
            'payment_datetime' => 'Дата и время выплаты',
            'qiwi_number' => 'Номер QIWI',
            'qiwi_answer' => 'Ответ от киви',
            'card_number' => 'Номер карты',
            'transaction_id' => 'ID транзакции в системе QIWI',
        ];
    }

    public function beforeSave($insert)
    {
        $this->owner_payment_id = Yii::$app->user->id;

        if (!$this->amount || $this->amount == 0) {
            $this->addError('error', 'Не указана сумма выплаты');
        }

        Yii::info('Тип выплаты: ' . $this->type_payment);

        if ($this->type_payment == 1) {
            $this->qiwi_number = Driver::find()->where(['id' => $this->driver_id])->one()->qiwi_number;
            if (!$this->qiwi_number) { //Если номер киви не задан в настройках и выбран в качестве оплаты
                $this->addError('error', 'Не найден номер QIWI. Проверьте номер QIWI-кошелька водителя');
            }
        } else if ($this->type_payment == 2) {
            $this->card_number = Driver::find()->where(['id' => $this->driver_id])->one()->card_number;
            if (!$this->card_number) { //Если номер карты не задан в настройках и выбран в качестве оплаты
                $this->addError('error', 'Не найден номер карты. Проверьте номер карты водителя');
            }
        }

//        if ($this->errors) {
//            return false;
//        }


//        $this->qiwi_number = $qiwi_number;

        $check = $this->checkAmount($this->amount, $this->driver_id);


        if ($check == 'true' && !$this->errors) { //Если лимиты не превышены и нет ошибок валидации платежных данных
            return parent::beforeSave($insert);
        } elseif ($check != 'true') { //Если превышены лимиты
            $this->addError('error', $check);
            return false;
        } else { //Если есть ошибки валидации платежных данных
            return false;
        }

    }

    public function afterSave($insert, $changedAttributes)
    {
        //При добавлении записи пишем в лог
        if ($insert) {
            $log = new Log();
//            $log->date = date('Y-m-d H:i:s', date());
            $log->partner_id = Partner::getPartnerId($this->owner_payment_id);
            $log->user_id = $this->owner_payment_id;
            $log->user_ip = Yii::$app->request->userIP;
            $log->description = $this->getDescription($this);
            $log->result = 'Тестовый результат';
            $log->amount = $this->amount;
            $log->payee = $this->driver_id;
            if (!$log->save()) {
                Yii::error($log->errors);
            };
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Проверка условий для вылпачиваемой суммы
     *
     * @param double $amount Сумма выплаты
     * @param int $driver_id ID водителя
     * @return bool|string
     */
    public function checkAmount($amount, $driver_id)
    {
        $partner_id = Users::find()->where(['id' => Yii::$app->user->id])->one()->partner_id;
        $partner_model = Partner::findOne(['id' => $partner_id]);
        $month_limit = $partner_model->month_limit;
        $month_amount = $this->getMonthAmount($driver_id);

        Yii::info('ID водителя: ' . $driver_id);
        Yii::info('Вылпчено за месяц: ' . $month_amount);

        if ($month_amount > $month_limit) {
            Yii::warning('Превышение месячного лимита', __METHOD__);
            return "Перевод невозможен! Превышен месячный лимит переводов (Лимит: {$month_limit} руб.).";
        } elseif (($month_amount + $amount) > $month_limit) {
            Yii::warning('Превышение месячного лимита', __METHOD__);
            $sum = $month_limit - $month_amount; //Возможный платеж без превышения месячного лимита
            return "Перевод невозможен! Превышение месячного лимита. Доступно для перевода {$sum} руб.";
        }

        $day_limit = $partner_model->day_limit;
        $day_amount = $this->getDayAmount($driver_id);

        Yii::info('Выплачено за день: ' . $day_amount);
        if ($day_amount > $day_limit) {
            Yii::warning('Превышение дневного лимита', __METHOD__);
            return "Перевод невозможен! Превышен суточный лимит переводов (Лимит: {$day_limit} руб.).";
        } elseif (($day_amount + $amount) > $day_limit) {
            $sum = $month_limit - $month_amount; //Возможный платеж без превышения месячного лимита
            return "Перевод невозможен! Превышение суточного лимита. Доступно для перевода {$sum} руб.";
        }

        $transaction_limit = $partner_model->transaction_limit;

        Yii::info('Лимит транзакции: ' . $transaction_limit, __METHOD__);

        if ($transaction_limit < $amount) {
            Yii::warning('Превышение лимита транзакции', __METHOD__);
            $this->addError('error', "Перевод невозможен! Превышен сумма разового перевода (Лимит: {$transaction_limit} руб.).");
            return "Перевод невозможен! Превышен сумма разового перевода (Лимит: {$transaction_limit} руб.).";
        }

        if ($amount > $partner_model->balance) {
            return "Недостаточно стредств (Текущий баланс {$partner_model->balance} руб.)";
        }

        return true;
    }

    /**
     * Получение общей суммы выплат водителя за месяц
     *
     * @param int $driver_id ID водителя
     * @return mixed
     */
    private function getMonthAmount($driver_id)
    {
        Yii::info('ID водителя: ' . $driver_id, __METHOD__);

//        $amount = Payment::find()
//            ->leftJoin('users', 'users.id = payment.owner_payment_id')
//            ->where(['users.partner_id' => $partner_id])
//            ->andWhere(['>=', 'create_datetime', date('Y-m-' . 01 . ' H:i:s', time())])
//            ->andWhere(['<=', 'create_datetime', date('Y-m-d H:i:s', time())])
//            ->sum('amount');
        $amount = Payment::find()->where(['driver_id' => $driver_id])
            ->andWhere(['>=', 'create_datetime', date('Y-m-' . 01 . ' 00:00:00', time())])
            ->andWhere(['<=', 'create_datetime', date('Y-m-d H:i:s', time())])
            ->sum('amount');
        if ($amount) {
            return $amount;
        } else {
            return 0;
        }
    }

    /**
     * Получение общей суммы выплат за сутки
     *
     * @param int $driver_id ID водителя
     * @return mixed
     */
    private function getDayAmount($driver_id)
    {
//        $amount = Payment::find()
//            ->leftJoin('users', 'users.id = payment.owner_payment_id')
//            ->where(['users.partner_id' => $partner_id])
//            ->andWhere(['>=', 'create_datetime', date('Y-m-d 00:00:00', time())])
//            ->andWhere(['<=', 'create_datetime', date('Y-m-d H:i:s', time())])
//            ->sum('amount');
        $amount = Payment::find()
            ->where(['driver_id' => $driver_id])
            ->andWhere(['>=', 'create_datetime', date('Y-m-d 00:00:00', time())])
            ->andWhere(['<=', 'create_datetime', date('Y-m-d H:i:s', time())])
            ->sum('amount');
        if ($amount) {
            return $amount;
        } else {
            return 0;
        }
    }

    /**
     * Запрос QIWI баланса
     * @param string $method
     * @return array
     */
    public static function getQiwiBalance($method = 'GET')
    {
        $phone = Settings::getSettingValueByKey('qiwi_phone_number');
        $api_key = Settings::getSettingValueByKey('qiwi_token');
        $api_url = [
            "https://edge.qiwi.com/funding-sources/v2/persons/{$phone}/accounts",
        ];
        $ch = curl_init(implode('/', $api_url));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/json",
            "Authorization: Bearer {$api_key}",
        ]);
        $res = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($res) {
            $res = json_decode($res, true);
        }
        $result = [
            'result' => $res,
            'query_error' => $err
        ];

        Yii::info($result);

//        return $res;

        if (!$err) {
            foreach ($res['accounts'] as $account) {
                if ($account['hasBalance']) {
                    return $account['balance']['amount'];
                }
            }
            return null;
        } else {
            return $err;
        }

    }

    /**
     * Проверка баланса в Ситимобил
     *
     * @param $outer_id
     * @return bool|mixed|string
     */
    public static function getCityBalance($outer_id)
    {
        $params = [
            'id_driver' => $outer_id,
        ];

        $res = ClientController::requestAPI('getDriverData', $params);
        return $res['data']['drivers'][0]['balance'];
    }

    /**
     * Создание вылпаты
     *
     * @param string $phone tel:+71234567890 (^tel:\+\d{1,15}$)
     * @param string $amount 1.00 (^\d+(.\d{0,3})?$)
     * @param string $comment 255 char (^\.{0,255}$)
     * @return array
     */
    public static function createBill($phone, $amount, $comment)
    {
        $phone = str_replace(' ', '', $phone);

        $bill_id = 1000 * time();

        Yii::info('ID платежа: ' . $bill_id, __METHOD__);
        Yii::info('Account: ' . $phone, __METHOD__);

        $params = [
            'id' => $bill_id,
            'sum' => [
                'amount' => $amount,
                'currency' => 643,
            ],
            'paymentMethod' => [
                'type' => 'Account',
                'accountId' => 643
            ],
            'fields' => [
                'account' => $phone
            ],
            '$comment' => $comment,
        ];

        $result = self::restQuery($params);

        if ($result) {
            return $result;
        } else {
            return null;
        }

    }

    /**
     * Перевод на Qiwi кошелёк
     *
     * @param array $params параметры запроса
     * @return mixed
     */
    private static function restQuery($params)
    {
        $method = 'POST';
        $api_key = Settings::getSettingValueByKey('qiwi_token');
        $encode_params = json_encode($params);

        Yii::info($encode_params, __METHOD__);

        $ch = curl_init("https://edge.qiwi.com/sinap/api/v2/terms/99/payments");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($encode_params));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer {$api_key}",
        ]);
        $res = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($res) {
            $res = json_decode($res, true);
        }
        $result = [
            'result' => $res,
            'query_error' => $err
        ];

        Yii::info($result);

        return $res;
    }

    /**
     * Генерирует описание в зависимости от типа выплаты
     *
     * @param mixed $payment_model Модель выплаты
     * @return string
     */
    private function getDescription($payment_model)
    {
        Yii::info('Qiwi number: ' . $payment_model->qiwi_number);
        Yii::info('Card: ' . $payment_model->card_number);
        Yii::info('Type payment: ' . $payment_model->type_payment);

        return 'Выплата ' . TypePayment::findOne($payment_model->type_payment)->name;

    }

}
