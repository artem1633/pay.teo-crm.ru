<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CityPaymentSearch represents the model behind the search form about `app\models\CityPayment`.
 */
class CityPaymentSearch extends CityPayment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'id', 'amount', 'remains'], 'integer'],
            [['payment_datetime', 'owner_receipt'], 'safe'],
            [['percent_retention'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CityPayment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'payment_datetime' => $this->payment_datetime,
            'amount' => $this->amount,
            'remains' => $this->remains,
            'percent_retention' => $this->percent_retention,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'owner_receipt', $this->owner_receipt]);

        return $dataProvider;
    }
}
