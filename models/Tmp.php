<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tmp".
 *
 * @property int $id
 * @property int $code Код аутентификации
 * @property string $code_create_at Время генерации кода
 * @property int $user_id Код пользоавтеля (владелец кода аутентификации)
 * @property string $date_end_user_ban Дата окончания бана пользователя за множественный неверный ввод кода пользователя
 * @property int $num_wrong_code Количество неудачных попыток ввода кода атентификации
 */
class Tmp extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tmp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'user_id', 'num_wrong_code'], 'integer'],
            [['code_create_at', 'date_end_user_ban'], 'safe'],
            [['user_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код аутентификации',
            'code_create_at' => 'Время генерации кода',
            'user_id' => 'Код пользоавтеля (владелец кода аутентификации)',
            'date_end_user_ban' => 'Дата окончания бана пользователя за множественный неверный ввод кода пользователя',
            'num_wrong_code' => 'Количество неудачных попыток ввода кода атентификации',
        ];
    }

    public function beforeSave($insert)
    {
        $this->code = rand(10000, 99999);

        if ($this->num_wrong_code > 0){ //Если уже начат отсчет неправильных вводов кода
            $this->num_wrong_code += 1;
        }

        return parent::beforeSave($insert);
    }
}
