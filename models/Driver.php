<?php

namespace app\models;

use app\modules\api\controllers\ClientController;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property string $surname Фамилия
 * @property string $name Имя
 * @property string $patronymic Отчество
 * @property string $call_sign Позывной
 * @property string $qiwi_number номер QIWI кошелька
 * @property string $outer_id Внешний ID
 * @property string $partner_id ID партнера, к которому относиться водитель
 * @property int $card_number Номер карты
 * @property int $type_payment_id Код типа платежа. Киви, карта и т.п.
 * @property string $link_offer Ссылка на текст оферты
 * @property int $accept_offer Согласие с офертой
 */
class Driver extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_payment_id', 'card_number', 'partner_id', 'accept_offer'], 'integer'],
            [['surname', 'name', 'patronymic', 'call_sign', 'outer_id', 'qiwi_number'], 'string', 'max' => 255],
            [['link_offer'], 'safe'],
//            [['qiwi_number'], 'unique', 'message' => 'Номер уже используется'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'ФИО',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'call_sign' => 'Позывной',
            'qiwi_number' => 'Номер QIWI кошелька',
            'outer_id' => 'Внешний ID',
            'partner_id' => 'ID партнера',
            'card_number' => 'Номер карты',
            'type_payment_id' => 'Код типа платежа',
            'link_offer' => 'Ссылка на оферту',
            'accept_offer' => 'Согласие с офертой',
        ];
    }

    /**
     * Список водителей партнера, к которому принадлежит текущий пользователь
     * @param $user_id
     * @return array
     */
    public static function getDriversByPartner($user_id)
    {
        $partner_id = Users::find()->where(['id' => $user_id])->one()->partner_id;
        $drivers = Driver::find()->where(['partner_id' => $partner_id])->all();

        return ArrayHelper::map($drivers, 'id', 'surname');
    }



    /**
     * Список водителей партнера, к которому принадлежит текущий пользователь без водителей, не принявших условие оферты
     * @param $user_id
     * @return array
     */
    public static function getDriversByPartnerWithoutNotAcceptedOffer($user_id)
    {
        $partner_id = Users::find()->where(['id' => $user_id])->one()->partner_id;
        $drivers = Driver::find()
            ->where(['partner_id' => $partner_id])
            ->andWhere(['=', 'accept_offer', 1])
            ->all();

        Yii::info($drivers, __METHOD__);

        return ArrayHelper::map($drivers, 'id', 'surname');
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert){

            $text = Settings::getSmsText($this->id);

            Yii::info($text, __METHOD__);

            ClientController::requestSMS($this->qiwi_number, $text);
        }
    }

//    /**
//     * Текст оферты пользователя партнера, создавшего водителя
//     *
//     * @param int $partner_id ID партнера
//     * @return mixed
//     */
//    public static function getOfferText($partner_id){
//        return Users::find()->where(['id' => $partner_id])->one()->text_offer;
//    }

    /**
     * Получает модель водителя по ID
     * @param int $id ID водителя
     * @return Driver|null
     */
    public static function getDriver($id){
       return self::findOne($id);
    }

    /**
     * @param int $id ID водителя
     * @return string Номер телефона в формате +79999999999
     */
    public static function getDriverPhone($id) {
        $phone = self::getDriver($id)->qiwi_number;
        $phone = str_replace(' ', '', $phone);

        Yii::info('Номер телефона: ' . $phone, __METHOD__);

        return $phone;

    }

}
