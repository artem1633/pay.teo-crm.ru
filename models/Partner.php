<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "partner".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $outer_city_id ID из сервиса CityMobil
 * @property string $api_key АПИ ключ от CityMobil
 * @property double $percent Процент ...
 * @property int $time_to_payment Время до зачисления (в часах) ...
 * @property int $month_limit Месячный лимит суммы водителя ...
 * @property int $day_limit Суточный лимит суммы водителя ...
 * @property int $transaction_limit Лимит операции водителя ...
 * @property int $max_payment Максимальная сумма платежа ...
 * @property int $access_to_payments Доступ к выплатам (да/нет)
 * @property double $amount_percent Процент от суммы ...
 * @property int $balance Баланс
 * @property int $waiting_recharge Баланс ожидает поступления
 * @property int $min_city_balance Минимальный баланс в Ситимобил
 * @property int $min_qiwi_balance Минимальный баланс в Qiwi
 */
class Partner extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['balance', 'day_limit', 'transaction_limit', 'max_payment', 'min_qiwi_balance', 'min_city_balance', 'percent', 'amount_percent'], 'number'],
            [['time_to_payment', 'month_limit', 'access_to_payments', 'waiting_recharge'], 'integer'],
            [['name', 'outer_city_id', 'api_key'], 'string', 'max' => 255],
            [['outer_city_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'outer_city_id' => 'ID из сервиса CityMobil',
            'api_key' => 'АПИ ключ от CityMobil',
            'percent' => 'Процент',
            'time_to_payment' => 'Время до зачисления (в часах)',
            'month_limit' => 'Месячный лимит суммы водителя',
            'day_limit' => 'Суточный лимит суммы водителя',
            'transaction_limit' => 'Лимит операции водителя',
            'max_payment' => 'Максимальная сумма платежа',
            'access_to_payments' => 'Доступ к выплатам (да/нет)',
            'amount_percent' => 'Процент от суммы',
            'balance' => 'Баланс',
            'waiting_recharge' => 'Баланс ожидает поступления',
            'min_city_balance' => 'Минимальный баланс в Ситимобил',
            'min_qiwi_balance' => 'Минимальный баланс в Qiwi',
        ];
    }


    public function beforeSave($insert)
    {
        $this->month_limit = round($this->month_limit, 2);
        $this->day_limit = round($this->day_limit, 2);
        $this->transaction_limit = round($this->transaction_limit, 2);
        $this->balance = round($this->balance, 2);


        Yii::info($this->month_limit, __METHOD__);
        Yii::info($this->day_limit, __METHOD__);
        Yii::info($this->transaction_limit, __METHOD__);
        Yii::info($this->balance, __METHOD__);

        return parent::beforeSave($insert);
    }

    /**
     * Список партнеров
     * @return array|ActiveRecord[]
     */
    public static function getPartners()
    {
        return self::find()->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getPartnerName($id)
    {
        return self::find()->where(['id' => $id])->one()->name;
    }

    /**
     * Возвращает модель партнера по ид пользователя
     * @param $user_id
     * @return array|null|ActiveRecord
     */
    public static function getPartnerModel($user_id)
    {
        $partner_id = Users::find()->where(['id' => $user_id])->one()->partner_id;
        return Partner::find()->where(['id' => $partner_id])->one();
    }

    /**
     * Возвращает ИД партнера по ИД пользователя
     * @param $user_id
     * @return mixed
     */
    public static function getPartnerId($user_id)
    {
        return Users::find()->where(['id' => $user_id])->one()->partner_id;
    }

    /**
     * Полчает ID партнера по ID водителя
     *
     * @param int $driver_id ID водителя
     * @return string
     */
    public static function getPartnerByDriver($driver_id){
        return Driver::findOne($driver_id)->partner_id;
    }
}
