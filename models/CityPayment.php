<?php

namespace app\models;


use app\modules\api\controllers\ClientController;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "city_payment".
 *
 * @property int $id
 * @property string $payment_datetime Дата и время
 * @property int $amount Сумма
 * @property string $status Статус - Резерв/Зачислено
 * @property string $owner_receipt Плательщик
 * @property int $remains Остаток на момент зачисления
 * @property double $percent_retention Процент списания
 * @property string $to_balance Дата и время перевода из транзитного в основной баланс
 */
class CityPayment extends ActiveRecord
{
    const RESERVE_STATUS = 1;
    const TRANSFERRED_STATUS = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['to_balance', 'payment_datetime'], 'safe'],
            [['status'], 'integer'],
            [['percent_retention', 'amount', 'remains'], 'number'],
//            [['owner_receipt'], 'string', 'max' => 255],
            [['owner_receipt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_datetime' => 'Дата и время',
            'amount' => 'Сумма',
            'status' => 'Статус - Резерв/Зачислено',
            'owner_receipt' => 'Плательщик',
            'remains' => 'Остаток на момент зачисления',
            'percent_retention' => 'Процент списания',
            'to_balance' => 'Дата перевода в основной баланс',
        ];
    }

    /**
     * Проверка баланса партнера в Ситимобил
     *
     * @param int $partner_id ID партнера
     * @return float|string
     */
    public static function checkCitymobilBalance($partner_id)
    {

        $partner_model = Partner::find()->where(['id' => $partner_id])->one();
        $outer_id = $partner_model->outer_city_id;
//        $outer_id = '2674xc43e4a7fd9f95e20c34f24dda3803b25';

        Yii::info('Outer ID: ' . $outer_id, __METHOD__);

        //Проверка баланса в Ситимобил

        $params['id_driver'] = $outer_id;

        $result = ClientController::requestAPI('getDriverData', $params);


        $city_balance = floatval($result['data']['drivers'][0]['balance']);
//        $qiwi_balance = Payment::getQiwiBalance();

        Yii::info('Баланс в Ситимобил: ' . $city_balance);

        if ($city_balance < $partner_model->min_city_balance) {
            Yii::warning('Текущий Ситимобил-баланс недостаточен для проведения выплаты', __METHOD__);
            return 'Текущий Ситимобил-баланс недостаточен для проведения выплаты';
        }

//        if ($qiwi_balance < $partner_model->min_qiwi_balance){
//            return 'Текущий Qiwi-баланс недостаточен для проведения выплаты';
//        }

        //Перевод из сити мобил
        if ($city_balance > 0) {

            //Сохраняяем в базу перевод из СитиМобил
            $cp_model = new CityPayment();

            if ($partner_model->percent > 0) {
                $cp_model->amount = ($city_balance - (($city_balance / 100) * $partner_model->percent)); //вычитаем процент. Баланс в копейках
            } else {
                $cp_model->amount = $city_balance;
            }

            $cp_model->owner_receipt = $partner_model->id;
            $cp_model->percent_retention = $partner_model->percent;
            $cp_model->status = CityPayment::RESERVE_STATUS;
            $cp_model->remains = $partner_model->balance;
            $cp_model->to_balance = self::setDateTransferBalance(date('Y-m-d H:i:00', time()));

            Yii::info('Начисляемая сумма: ' . $cp_model->amount, __METHOD__);
            Yii::info('Создатель платежа: ' . $cp_model->owner_receipt, __METHOD__);
            Yii::info('Дата перечисления в основной баланс: ' . $cp_model->to_balance, __METHOD__);

            if ($cp_model->validate() && $cp_model->save()) {
                //Обнуляем баланс в Ситимобил
                $params['id_driver'] = $partner_model->outer_city_id;
                $params['type'] = 'minus';
                $params['price'] = $city_balance;
                $params['description'] = 'Перевод в PayPartner';

                Yii::info($params);

                $result_write_off = ClientController::requestAPI('changeDriverBalance', $params);

                //Проверяем результат обнуления баланса в СитиМобил

                if ($result_write_off['success'] == 0) {
                    Yii::error('Ошибка обнуления баланса Ситимобил', __METHOD__);
                    CityPayment::deleteAll(['id' => $cp_model->id]); //Удаляем запись о переводе
                }
            } else {
                Yii::error($cp_model->errors);
            }
        } else {
            return 'Нулевой баланс';
        }

        return $city_balance;

    }

    /**
     * Проверка и перевод баланса из "в ожидании" на текущий баланс партнера через указанное время
     * (есди не указан пользователь - проводятся балансы всех записей)
     */
    public static function checkWaitingBalance($owner_id = 0)
    {
        if ($owner_id = 0 && Users::isAdmin()) { //Проведение баланса для всех зачислений (только админ)
            $balances = CityPayment::find()->where(['status' => self::RESERVE_STATUS])->all();
        } elseif($owner_id != 0) {
            $balances = CityPayment::find()->where(['status' => self::RESERVE_STATUS])->andWhere(['owner_id' => $owner_id])->all();
        } else {
             return 'Ошибка перевода баланса.';
        }
//        $balances = CityPayment::find()->where(['status' => self::RESERVE_STATUS])->all();

        if ($balances) {
            //Проверяем дату начисления транзитного баланса и при необходимости переводим в основной баланс
            foreach ($balances as $balance) {
                $partner = Partner::find()->where(['id' => $balance->owner_receipt])->one();
//                $time_to_payment = $partner->time_to_payment; //Время до зачисления на основной баланс
                $payment_datetime = $balance->to_balance;

                Yii::info($payment_datetime);

                if (time() >= strtotime($payment_datetime)) {

                    Yii::info('Текущий баланс: ' . $partner->balance);
                    Yii::info('Сумма пополнения: ' . $balance->amount);
                    //Переводим на основной баланс
                    $partner->balance += $balance->amount;

                    if ($partner->validate() && $partner->save()) {
                        //Меняем статус на зачислено
                        $balance->status = self::TRANSFERRED_STATUS;

                        $balance->save();
                        VarDumper::dump($balance->errors, 10, true);
                    }
                } else {
                    Yii::info('Сумма ' . $balance->amount . 'руб. ожидает перевода на основной баланс');
                }
            }
        } else {
            Yii::warning('Не найдено сумм, ожидающих перевода');
        }

        return 'ok';
    }

    /**
     * Получение баланса "в ожидании" для партнера текущего пользоватя
     * @return int
     */
    public static function getWaitingBalance()
    {
        $partner_id = Users::find()->where(['id' => Yii::$app->user->id])->one()->partner_id;

        Yii::info('ID партнера: ' . $partner_id);

        $summ = 0;

        if ($partner_id) {
            $summ = CityPayment::find()
                ->where(['owner_receipt' => $partner_id])
                ->andWhere(['status' => self::RESERVE_STATUS])
                ->sum('amount');

        }

        Yii::info('Сумма в ожидании: ' . $summ);

        return $summ ? $summ : 0;
    }

        /**
     * Получает основной баланс партнера
     * @return int Баланс партнера
     */
    public static function getBalance()
    {
        $partner_id = Users::find()->where(['id' => Yii::$app->user->id])->one()->partner_id;
        return Partner::findOne($partner_id)->balance;
    }

    /**
     * Определение, согласно условиям, даты перевода суммы в основной баланс
     *
     * @param string $date_payment Дата начилсения в транзитный баланс
     * @return false|string
     */
    private static function setDateTransferBalance($date_payment)
    {
        $day_of_week = date('N', strtotime($date_payment));
        $date = date_create($date_payment);

        Yii::info('Текущий день недели: ' . $day_of_week);
//        VarDumper::dump(date_format(date_modify($date, '+2 days'),'Y-m-d 17:00:00')); die();
        //Если зачисление в субботу или воскресенье
        if ($day_of_week == 6 || $day_of_week == 7) {
            switch ($day_of_week){
                case 6:
                    return date_format(date_modify($date, '+2 days'),'Y-m-d 17:00:00');
                case 7:
                    return date_format(date_modify($date, '+1 day'),'Y-m-d 17:00:00');
            }
        }

        //Если оплата до 11:00 текщего дня
        if ($date_payment < date('Y-m-d 11:00:01', time())) {
            return date('Y-m-d 17:00:00', strtotime($date_payment));
        } elseif ($date_payment > date('Y-m-d 11:00:01', time())){
            return date_format(date_modify($date, '+1 day'),'Y-m-d 17:00:00');
        }
        return false;
    }
}
