<?php

namespace app\models;

use yii\helpers\Url;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $key Ключ
 * @property string $value Значение
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'key', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'key' => 'Ключ',
            'value' => 'Значение',
        ];
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function getSettingValueByKey($key)
    {
        return Settings::find()->where(['key' => $key])->one()->value;
    }

    /**
     * Получает и преобразует текст СМС сообщения для водителя (принятие условий оферты)
     * @param int $driver_id ID водителя
     * @return string
     */
    public static function getSmsText($driver_id){
        $driver = Driver::findOne($driver_id);

        $url = Url::toRoute(['driver/offer', 'id' => $driver->id], true);
        $text = Settings::getSettingValueByKey('driver_sms_text');

        $text = str_replace('{фио_водителя}', $driver->surname, $text);
        $text = str_replace('{ссыслка}', $url, $text);

        return $text;
    }
}
