<?php

namespace app\models;


/**
 * This is the model class for table "import_error".
 *
 * @property int $id
 * @property string $created_at Дата создания записи
 * @property int $user_id ID пользователя, выполняющего импорт
 * @property string $user_ip IP адрес пользователя, выполняющего импорт
 * @property string $description Описание ошибки
 * @property int $driver_id ID водителя (получателя выплаты)
 * @property double $amount Сумма выплаты
 * @property int $type_payment Тип выплаты
 *
 * @property Users $user
 * @property Driver $driver
 */
class ImportError extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'import_error';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['user_id', 'driver_id', 'type_payment'], 'integer'],
            [['description'], 'string'],
            [['amount'], 'number'],
            [['user_ip'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания записи',
            'user_id' => 'ID пользователя, выполняющего импорт',
            'user_ip' => 'IP адрес пользователя, выполняющего импорт',
            'description' => 'Описание ошибки',
            'driver_id' => 'ID водителя (получателя выплаты)',
            'amount' => 'Сумма выплаты',
            'type_payment' => 'Тип выплаты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }
}
