<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tmp;

/**
 * TmpSearch represents the model behind the search form about `app\models\Tmp`.
 */
class TmpSearch extends Tmp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'code', 'user_id', 'num_wrong_code'], 'integer'],
            [['code_create_at', 'date_end_user_ban'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tmp::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'code' => $this->code,
            'code_create_at' => $this->code_create_at,
            'user_id' => $this->user_id,
            'date_end_user_ban' => $this->date_end_user_ban,
            'num_wrong_code' => $this->num_wrong_code,
        ]);

        return $dataProvider;
    }
}
