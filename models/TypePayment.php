<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type_payment".
 *
 * @property int $id
 * @property string $name
 *
 * @property Driver[] $drivers
 */
class TypePayment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['type_payment_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public static function getTypePayments(){
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
