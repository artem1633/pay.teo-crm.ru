$(function(){
    $('#btn-1, #btn-3').click(function(){
        // debugger;
        $('.inf').addClass('hidden');
        $('#btn-3').addClass('hidden');

        var username = $('#loginform-username').val();
        var pass = $('#loginform-password').val();

        $.ajax({
            url: "validate-user",
            type: 'POST',
            data: {
                username: username,
                password: pass
            },
            success: function(data){
                if (data != 'ok'){
                    // alert(data);
                    $('#info').text(data);
                    $('.inf').removeClass('hidden');
                } else {
                    $('#step-1').addClass('hidden');
                    $('#btn-1').addClass('hidden');
                    $('#step-2').removeClass('hidden');
                    $('#btn-2').removeClass('hidden');
                }
            }
        })
    });

    $('#btn-2').click(function(){
        $('.inf').addClass('hidden');

        var code = $('#loginform-code').val();
        var username = $('#loginform-username').val();
        var pass = $('#loginform-password').val();
        $.ajax({
            url: "validate-code",
            type: 'POST',
            data: {
                code: code,
                username: username,
                password: pass
            },
            success: function (data) {
                // alert(data);
                $('#info').text(data);
                $('.inf').removeClass('hidden');
                setTimeout(showResendBtn, 30000);
            }
        })
    });
});

function showResendBtn() {
    $('#btn-3').removeClass('hidden')
}