<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b><?= Yii::$app->name ?></b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Введите данные авторизации</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
        <div class="row">
            <div class="col-xs-12">
                <div id="step-1">
                    <?= $form
                        ->field($model, 'username', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => 'Логин'/*$model->getAttributeLabel('username')*/]) ?>

                    <?= $form
                        ->field($model, 'password', $fieldOptions2)
                        ->label(false)
                        ->passwordInput(['placeholder' => 'Пароль'/*$model->getAttributeLabel('password')*/]) ?>
                </div>
                <div id="step-2" class="hidden">
                    <?= $form
                        ->field($model, 'code', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => 'СМС код подтверждения']) ?>
                </div>
                <div class="inf hidden" style="color: red;">
                    <p id="info">Какая-то ошибка</p>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12">
                <?= Html::a('Вход', '#', [
                    'id' => 'btn-1',
                    'class' => 'btn btn-primary btn-block btn-flat'
                ]) ?>
                <?= Html::a('Подтвердить', '#', [
                    'id' => 'btn-2',
                    'class' => 'btn btn-primary btn-block btn-flat hidden'
                ]) ?>
                <?= Html::a('Повторить отправку СМС кода', '#', [
                    'id' => 'btn-3',
                    'class' => 'btn btn-primary btn-block btn-flat hidden'
                ]) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

        <!-- <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in
                using Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign
                in using Google+</a>
        </div> -->
        <!-- /.social-auth-links -->

        <!-- <a href="#">Забыл пароль</a><br> -->
        <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
<?php
$this->registerJsFile('/js/login2fa.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
?>