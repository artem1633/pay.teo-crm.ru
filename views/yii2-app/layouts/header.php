<?php

use app\models\CityPayment;
use app\models\Users;
use yii\helpers\Html;
use yii\bootstrap\Modal;

?>

    <header class="main-header">

        <?= Html::a('<span class="logo-mini">ВП</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu"
               role="button"><span class="sr-only">Toggle navigation</span> </a>
            <div class="nav-right">
                <div class="row">
                    <div class="col-md-2">Баланс: </div>
                    <div class="col-md-10 pull-left"><?= CityPayment::getBalance(); ?> руб.</div>
                    <div class="col-md-2"></div>
                    <div class="col-md-10"><?= CityPayment::getWaitingBalance(); ?> руб. в ожидании</div>

                </div>
                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs"><?= Users::getFullName(Yii::$app->user->identity->id) ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="<?= 'http://' . $_SERVER['SERVER_NAME'] ?>/images/nouser.png"
                                         class="img-circle" alt="User Image"/>
                                    <p> <?= Users::getFullName(Yii::$app->user->identity->id) ?> </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?= Html::a('Профиль', ['users/update', 'id' => Yii::$app->user->identity->id],
                                            ['role' => 'modal-remote', 'title' => 'Профиль', 'class' => 'btn btn-default btn-flat']); ?>
                                    </div>
                                    <?php // echo Html::a('Изменить пароль', ['users/change', 'id' => Yii::$app->user->identity->id],['role' => 'modal-remote', 'title' => 'Изменить пароль', 'class' => 'btn btn-default btn-flat']); ?>
                                    <div class="pull-right">
                                        <?= Html::a(
                                            'Выход',
                                            ['/site/logout'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>

        </nav>
    </header>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => false,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>