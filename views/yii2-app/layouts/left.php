<?php

use app\models\Users;

$permission = Yii::$app->user->identity->permission;
$id = Yii::$app->user->identity->id;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php

        if (Users::isAdmin()){
            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                [
                    'label' => 'Справочники',
                    'icon' => 'book',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Партнеры', 'icon' => 'file-text-o', 'url' => ['/partner'],],
                        ['label' => 'Водители', 'icon' => 'file-text-o', 'url' => ['/driver'],],
                        ['label' => 'Пользователи у партнеров', 'icon' => 'file-text-o', 'url' => ['/users'],],
                        ['label' => 'Выплаты', 'icon' => 'file-text-o', 'url' => ['/payment'],],
                        ['label' => 'Оплаты (Ситимобил)', 'icon' => 'file-text-o', 'url' => ['/city-payment'],],
                        ['label' => 'Логи операций', 'icon' => 'file-text', 'url' => ['/log'],],
                    ],
                ],
                ['label' => 'Выполнить зачисление', 'icon' => 'money', 'url' => ['/city-payment/check-waiting-balance']],
                ['label' => 'Настройки', 'icon' => 'file-text-o', 'url' => ['/settings']],
                ['label' => 'Профиль', 'icon' => 'file-text-o', 'url' => ['users/update', 'id' => Yii::$app->user->identity->id]],

            ];
        } else {
            $items = [
                ['label' => 'Меню', 'options' => ['class' => 'header']],
                ['label' => 'Профиль', 'icon' => 'file-text-o', 'url' => ['users/update', 'id' => Yii::$app->user->identity->id]],
                ['label' => 'Выплаты', 'icon' => 'file-text-o', 'url' => ['/payment']],
                ['label' => 'Выполнить зачисление', 'icon' => 'money', 'url' => ['/city-payment/check-owner-waiting-balance']],
//                ['label' => 'Загрузить из файла (Excel)', 'icon' => 'money', 'url' => ['/payment/upload']],
                ['label' => 'Пользователи', 'icon' => 'file-text-o', 'url' => ['/users']],
                ['label' => 'Водители', 'icon' => 'file-text-o', 'url' => ['/driver']],
            ];
        }



        if(isset(Yii::$app->user->identity->id))
            {
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => $items,
                    ]
                );
            }
        ?>

    </section>

</aside>
