<?php

use app\models\Partner;
use app\models\TypePayment;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'call_sign')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'outer_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'qiwi_number')->widget(MaskedInput::className(), [
                'mask' => '+7999 999 99 99',
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'card_number')->textInput() ?>
        </div>
<!--        <div class="col-md-4">-->
        <?php // echo $form->field($model, 'type_payment_id')->dropDownList(TypePayment::getTypePayments())->label('Оплата через:') ?>
<!--        </div>-->

        <?= $form->field($model, 'partner_id')->hiddenInput(['value' => Partner::getPartnerId(Yii::$app->user->id)])->label(false) ?>
    </div>





  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
