<?php

use app\assets\AppAsset;


$session = Yii::$app->session;

$body = $session['body'];

$this->context->layout = 'offer-layout';

?>

<div class="offer">
    <div class="offer-body">
        <?= $body?>
    </div>
</div>
<?php //\yii\helpers\VarDumper::dump($body, 10, true)?>
<?php //\yii\helpers\VarDumper::dump($model, 10, true)?>

<?php
//$script = <<< JS
//    $("#btn-accept-offer").click(function() {
//       $.ajax({
//        url: 'accept-offer',
//        type: 'POST',
//        data: 'driver=' + $('#driver').html(),
//        success: function (data) {
//            if (data == 'ok'){
//                $('.offer').html('<h1>Спасибо. Вы можете закрыть эту страницу</h1>');
//            } else {
//                alert(data);
//            }
//        }
//       });
//    });
//JS;
//$this->registerJs($script, yii\web\View::POS_END);
//?>
