<?php

use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'surname',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'patronymic',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'call_sign',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'qiwi_number',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'card_number',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'accept_offer',
        'format' => 'raw',
        'value' => function ($data) {
            if ($data->accept_offer == 1) {
                return Html::tag('div', 'Условия приняты', ['style' => 'color:green']);
            } else {
                return Html::tag('div', 'Условия не приняты', ['style' => 'color: red']);
            }


        }
    ],

//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'outer_id',
//     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{view} {update} {send} {delete}',
        'visible' => !Users::isReadOnly(),
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Подтвердите удаление елемента'],
        'buttons' => [
            'send' => function ($url, $model) {
                $custom_url = Url::to(['/driver/send-sms-to-driver', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-repeat"></span>', $custom_url,
                    ['title' => Yii::t('yii', 'Отправить СМС о принятии оферты')]);
            }
        ],
    ],

];   