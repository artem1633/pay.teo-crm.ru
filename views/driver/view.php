<?php

use app\models\Users;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
/* @var $paymentDataProvider app\models\Payment */
?>
<div class="driver-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'surname',
//            'name',
//            'patronymic',
            'call_sign',
            'qiwi_number',
            'card_number',
            'outer_id',
            [
                'attribute' => 'accept_offer',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->accept_offer == 1) {
                        return Html::tag('div', 'Условия приняты', ['style' => 'color:green']);
                    } else {
                        return Html::tag('div', 'Условия не приняты', ['style' => 'color: red']);
                    }
                }
            ],
        ],
    ]) ?>

</div>

<div class="driver-payments">
    <?=GridView::widget([
        'id'=>'crud-datatable',
        'dataProvider' => $paymentDataProvider,
//        'filterModel' => $searchModel,
        'pjax'=>true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'create_datetime',
                'format' => 'date',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'amount',
                'value' => function($data){
                    return $data->amount . ' руб.';
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'owner_payment_id',
                'value' => function($data){
                    return Users::find()->where(['id' => $data->owner_payment_id])->one()->surname;
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'status',
            ],
        ],
        'toolbar'=> [
            ['content'=>
//                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
//                    ['role'=>'modal-remote','title'=> 'Create new Drivers','class'=>'btn btn-default']).
//                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                '{toggleData}'.
                '{export}'
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'type' => 'primary',
            'heading' => '<i class="glyphicon glyphicon-list"></i> Список выплат',
//                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
//            'after'=>BulkButtonWidget::widget([
//                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
//                        ["bulk-delete"] ,
//                        [
//                            "class"=>"btn btn-danger btn-xs",
//                            'role'=>'modal-remote-bulk',
//                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                            'data-request-method'=>'post',
//                            'data-confirm-title'=>'Are you sure?',
//                            'data-confirm-message'=>'Are you sure want to delete this item'
//                        ]),
//                ]).
                '<div class="clearfix"></div>',
        ]
    ])?>
</div>
