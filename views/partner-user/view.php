<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PartnerUser */
?>
<div class="partner-user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'surname',
            'name',
            'patronymic',
            'phone',
            'accept_offer',
            'link_offer:ntext',
            'text_offer:ntext',
        ],
    ]) ?>

</div>
