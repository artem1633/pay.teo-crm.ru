<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tmp */
?>
<div class="tmp-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'code_create_at',
            'user_id',
            'date_end_user_ban',
            'num_wrong_code',
        ],
    ]) ?>

</div>
