<?php

use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php $this->registerCsrfMetaTags() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Оферта</title>
        <meta property="og:url" content="http://taxi1990.ru/oferta"/>
        <meta property="og:title" content="Оферта"/>
        <meta property="og:description" content=""/>
        <meta property="og:type" content="website"/>
        <meta property="fb:app_id" content="257953674358265"/>
        <meta name="format-detection" content="telephone=no"/>
        <meta http-equiv="x-dns-prefetch-control" content="on">
        <link rel="dns-prefetch" href="https://tilda.ws">
        <link rel="dns-prefetch" href="https://static.tildacdn.com">
        <link rel="canonical" href="http://taxi1990.ru/oferta">
        <link rel="shortcut icon" href="https://static.tildacdn.com/tild6638-3538-4364-b766-626332646139/favicon.ico"
              type="image/x-icon"/><!-- Assets -->
        <link rel="stylesheet" href="https://static.tildacdn.com/css/tilda-grid-3.0.min.css" type="text/css"
              media="all"/>
        <link rel="stylesheet" href="https://tilda.ws/project710468/tilda-blocks-2.12.css?t=1544270637" type="text/css"
              media="all"/>
        <link rel="stylesheet" href="https://static.tildacdn.com/css/tilda-animation-1.0.min.css" type="text/css"
              media="all"/>
        <script type="text/javascript"
                src="http://gc.kis.v2.scr.kaspersky-labs.com/B3A2CD05-385B-014F-A56F-AC3D776EE125/main.js"
                charset="UTF-8"></script>
        <script src="https://static.tildacdn.com/js/jquery-1.10.2.min.js"></script>
        <script src="https://static.tildacdn.com/js/tilda-scripts-2.8.min.js"></script>
        <script src="https://tilda.ws/project710468/tilda-blocks-2.7.js?t=1544270637"></script>
        <script src="https://static.tildacdn.com/js/lazyload-1.3.min.js" charset="utf-8"></script>
        <script src="https://static.tildacdn.com/js/tilda-animation-1.0.min.js" charset="utf-8"></script>
        <script type="text/javascript">window.dataLayer = window.dataLayer || [];</script>
        <?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>
    <body>
    <?= $this->render(
        '/driver/offer'
    ) ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>