<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ImportError */
?>
<div class="import-error-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'user_id',
            'user_ip',
            'description:ntext',
            'driver_id',
            'amount',
            'type_payment',
        ],
    ]) ?>

</div>
