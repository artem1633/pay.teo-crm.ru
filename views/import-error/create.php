<?php



/* @var $this yii\web\View */
/* @var $model app\models\ImportError */

?>
<div class="import-error-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
