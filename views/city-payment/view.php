<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CityPayment */
?>
<div class="city-payment-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'payment_datetime',
            'amount',
            'status',
            'owner_receipt',
            'remains',
            'percent_retention',
        ],
    ]) ?>

</div>
