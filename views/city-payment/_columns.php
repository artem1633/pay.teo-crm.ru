<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_datetime',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'amount',
        'label' => 'Сумма (руб.)',
        'value' => function($data){
                return $data->amount;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'label' => 'Статус',
        'format' => 'raw',
        'value' => function($data){
            switch ($data->status){
                case 1:
                    $btn = \yii\helpers\Html::a('Зачислить',
                        Url::to(['city-payment/manual-transfer',
                            'id' => $data->id]),[
                                'class' => 'btn btn-info btn-sm',
                        ]);

                    return 'Резерв ' . $btn;
                case 2:
                    return 'Зачислено';
                    break;
                default:
                    return 'Не определено.';
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'owner_receipt',
        'value' => function($data){
            return \app\models\Partner::getPartnerName($data->owner_receipt);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'remains',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'percent_retention',
     ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Are you sure?',
//                          'data-confirm-message'=>'Are you sure want to delete this item'],
//    ],

];   