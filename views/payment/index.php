<?php

use app\models\ImportError;
use app\models\Partner;
use app\models\Users;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$access_to_payments = Partner::findOne(['id' => Partner::getPartnerId(Yii::$app->user->id)])->access_to_payments;

if (Users::isAdmin() || $access_to_payments == 1) {
    $add_btn = Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
        ['role' => 'modal-remote', 'title' => 'Создать новый платёж', 'class' => 'btn btn-default']);
} else {
    $add_btn = '';
}
//Ищем необработанные ошибки импорта в разрезе пользователя
$query = ImportError::find()->where(['user_id' => Yii::$app->user->id])->all();
if ($query){
    Yii::info('Есть ошибки', __METHOD__);
    $error_btn = Html::a('Ошибки импорта', \yii\helpers\Url::to(['/import-error/']), ['class' => 'btn btn-danger']);
} else {
    Yii::info('Нет ошибок', __METHOD__);
    $error_btn = '';
}

?>
<div class="payment-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'exportConfig' => [
                GridView::EXCEL => [
                    'label' => 'Сохранить в EXCEL',
                    'filename' => 'Список выплат водителям',
                ],

            ],
            'toolbar' => [
                ['content' =>
                    $add_btn .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сброс таблицы']) .
                    '{toggleData}' .
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                    'before' => Html::a('Импорт из файла', ['/payment/upload'], [
                        'role' => 'modal-remote',
                        'title' => 'View',
                        'data-toggle' => 'tooltip',
                        'class' => 'btn btn-primary'
                    ]) .
                    $error_btn,
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Платежи',
//                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                'after' => '<div class="clearfix"></div>',
            ],
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
