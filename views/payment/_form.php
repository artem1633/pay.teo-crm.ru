<?php

use app\models\Driver;
use app\models\Payment;
use app\models\TypePayment;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-md-6">
            <?php //echo $form->field($model, 'driver_id')->dropDownList(Driver::getDriversByPartner(Yii::$app->user->id))->label('Водитель') ?>
            <?= $form->field($model, 'driver_id')->widget(Select2::classname(), [
                'data' => Driver::getDriversByPartnerWithoutNotAcceptedOffer(Yii::$app->user->id),
                'options' => [
                    'placeholder' => 'Выберите водителя',
                    'multiple' => false,
//                    'disabled' => $disabled,
//                    'onchange' => '$.post(
//                                        "' . Url::toRoute('/driver/info-for-payment') . '",
//                                        {driver_id: $(this).val()},
//
//                                        function(res){
//                                            var result = jQuery.parseJSON(res);
//                                            if (result[0] == "QIWI"){
//                                                $("#qiwi").removeClass("hidden");
//                                                $("#qiwi input").val(result[1]);
//                                            } else if(result[0] == "Карта") {
//                                                $("#card").removeClass("hidden");
//                                                $("#card").val(result[1]);
//                                            }
//                                        }
//                                        );'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Водитель'); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'amount')->textInput([
                    'placeholder' => 'Сумма выплаты',
                    'value' => $model->isNewRecord ? null : $model->amount,
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php $items = ArrayHelper::map(TypePayment::find()->all(), 'id', 'name') ?>
            <?= $form->field($model, 'type_payment')->widget(Select2::classname(), [
                'data' => $items,
                'options' => [
                    'placeholder' => 'Выберите тип выплаты',
                    'multiple' => false,
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Выплата на '); ?>
        </div>

        <div id="qiwi" class="col-md-4 hidden">
            <?= $form->field($model, 'qiwi_number')->textInput(['disabled' => true]) ?>
        </div>
        <div id="card" class="col-md-4 hidden">
            <?= $form->field($model, 'card_number')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'owner_payment_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>






    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
