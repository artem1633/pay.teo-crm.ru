<?php

/* @var $this yii\web\View */
/* @var $model app\models\Payment */

?>
<div class="payment-create">
    <?= $this->render('_import_form', ['model' => $model]) ?>
</div>
