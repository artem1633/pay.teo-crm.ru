<?php

use app\models\Driver;
use app\models\Users;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
?>
<div class="payment-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'create_datetime:date',
            [
                'attribute' =>'driver_id',
                'label' => 'Водитель',
                'value' => function($data){
                    return Driver::findOne(['id' => $data->driver_id])->surname;
                }
            ],
            [
                    'attribute' => 'amount',
                    'value' => $model->amount . ' руб.'
            ],
            'status',
            [
                    'attribute' => 'owner_payment_id',
                    'value' => function($data){
                        return Users::findOne(['id' => $data->owner_payment_id])->surname;
                    }
            ],
            'payment_datetime:date',
            'qiwi_number',
            'qiwi_answer',
        ],
    ]) ?>

</div>
