<?php

use app\models\Driver;
use app\models\User;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
        'headerOptions' => ['class' => 'skip-export'],
        'contentOptions' => ['class' => 'skip-export'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
        'hidden' => true,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'create_datetime',
        'format' => 'datetime',
        'headerOptions' => ['class' => 'skip-export'],
        'contentOptions' => ['class' => 'skip-export'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'type_payment',
        'label' => 'Тип платежа',
        'value' => function ($data) {
            if ($data->qiwi_number){
                return 'QIWI';
            } elseif ($data->card_number){
                return 'Карта';
            } else {
                return 'Не указан';
            }
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'driver_id',
        'label' => 'Водитель',
        'headerOptions' => ['class' => 'skip-export'],
        'contentOptions' => ['class' => 'skip-export'],
        'value' => function ($data) {
            return Driver::find()->where(['id' => $data->driver_id])->one()->surname;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'amount',
        'value' => function ($data) {
            return $data->amount . ' руб.';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'headerOptions' => ['class' => 'skip-export'],
        'contentOptions' => ['class' => 'skip-export'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'owner_payment_id',
        'visible' => User::isAdmin(),
        'headerOptions' => ['class' => 'skip-export'],
        'contentOptions' => ['class' => 'skip-export'],
        'value' => function ($data) {
            return User::find()->where(['id' => $data->owner_payment_id])->one()->surname;
        }
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_datetime',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'qiwi_number',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'qiwi_answer',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'visible' => !Users::isReadOnly(),
        'dropdown' => false,
        'vAlign' => 'middle',
//        'template' => '',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'update' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-pencil"></span>',
                    $url,
                    [
                        'title' => 'Download',
                        'data-pjax' => '0',
                        'hidden' => $model->status,
                        'role' => 'modal-remote',
                        'data-toggle' => 'tooltip',
                    ]
                );
            },
            'delete' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-trash"></span>',
                    $url,
                    [
                        'title' => 'Download',
                        'data-pjax' => '0',
                        'hidden' => $model->status,
                        'role' => "modal-remote",
                        'data-request-method' => "post",
                        'data-toggle' => "tooltip",
                        'data-confirm-title' => "Вы уверены?",
                        'data-confirm-message' => "Подтвердите удаление елемента"
                    ]
                );
            },
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
    ],

];   