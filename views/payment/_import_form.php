<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="import-payment-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <!--    --><?php //\yii\helpers\VarDumper::dump($model, 10, true); die(); ?>

    <!--    --><?php //echo $form->errorSummary($model); ?>


    <div class="row" style="display: flex; align-items: center;">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->widget(FileInput::classname([
                'options' => ['multiple' => true],
            ]))->label('Выберите файл для импорта'); ?>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <? //= Html::submitButton('Импортировать', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
