<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Partner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-form">

    <?php $form = ActiveForm::begin([
//        'fieldConfig' => [
//            'enableLabel' => false,
//        ]
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Наименование']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'outer_city_id')->textInput(['maxlength' => true, 'placeholder' => 'ID сервиса CityMobil']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'api_key')->textInput(['maxlength' => true, 'placeholder' => 'АПИ ключ CityMobil']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'percent')->textInput(['placeholder' => 'Процент']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'time_to_payment')->textInput(['placeholder' => 'Часов до зачисления']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'month_limit')->textInput([
                'placeholder' => 'Месячный лимит суммы водителя',
                'value' => $model->isNewRecord ? null : $model->month_limit,
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'day_limit')->textInput([
                'placeholder' => 'Суточный лимит суммы водителя',
                'value' => $model->isNewRecord ? null : $model->day_limit,
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'transaction_limit')->textInput([
                'placeholder' => 'Разовый лимит операции',
                'value' => $model->isNewRecord ? null : $model->transaction_limit,
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'balance')->textInput([
                'placeholder' => 'Баланс',
                'value' => $model->isNewRecord ? null : $model->balance,
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'min_city_balance')->textInput(['placeholder' => 'Минимальный баланс в Ситимобил']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'access_to_payments')->checkbox(['label' => 'Доступ к выплатам'])->label(true) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
