<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Partner */
?>
<div class="partner-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'outer_city_id',
            'api_key',
            [
                    'attribute' => 'percent',
                    'value' => $model->percent . '%',
            ],
            [
                'attribute' => 'time_to_payment',
                'label' => 'Время до зачисления',
                'value' => $model->time_to_payment . ' ч.',
            ],
            [
                'attribute' => 'month_limit',
                'value' => ($model->month_limit) . ' руб.',
            ],
            [
                'attribute' => 'day_limit',
                'value' => $model->day_limit . ' руб.',
            ],
            [
                'attribute' => 'transaction_limit',
                'value' => $model->transaction_limit . ' руб.',
            ],
            [
                    'attribute' => 'access_to_payments',
                    'label' => 'Доступ к выплатам',
                    'format' => 'raw',
                    'value' => function($data) {
                        if ($data->access_to_payments == 1){
                            return Html::tag('div', 'Да', ['style' => 'color:green']);
                        } else {
                            return Html::tag('div', 'Нет', ['style' => 'color:red']);
                        }
                    }
            ],
            [
                'attribute' =>  'balance',
                'value' => $model->balance . ' руб.',
            ],
        ],
    ]) ?>

</div>
