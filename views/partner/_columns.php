<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'outer_city_id',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'api_key',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'percent',
        'value' => function($data){
            return $data->percent . '%';
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'time_to_payment',
//    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'month_limit',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'day_limit',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'transaction_limit',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'max_payment',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'access_to_payments',
         'format' => 'raw',
         'value' => function($data){
            switch ($data->access_to_payments){
                case 1:
                    return Html::tag('div', 'Выплаты разрешены', ['style' => 'color:green']);
                    break;
                case 0:
                    return Html::tag('div', 'Выплаты запрещены', ['style' => 'color:red']);
                    break;
                default:
                    return Html::tag('div', 'Доступ не определен', ['style' => 'color:orange']);
            }
         }
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'amount_percent',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'balance',
         'value' => function($data){
            return $data->balance . ' руб.';
         }
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'waiting_recharge',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   