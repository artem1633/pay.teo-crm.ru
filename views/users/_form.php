<?php

use app\models\Partner;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= Partner::find()->where(['id' => $model->partner_id])->one()->name ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                'mask' => '+7 (999) 999 99 99',
            ]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php // $form->field($model, 'accept_offer')->checkbox() ?>
        </div>
        <div class="col-md-4">
            <?php // echo $form->field($model, 'link_offer')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php // echo $form->field($model, 'text_offer')->textarea() ?>
        </div>
        <div class="col-md-4">
            <?php if (Users::isAdmin()) { ?>
                <?= $form->field($model, 'partner_id')->dropDownList([ArrayHelper::map(Partner::getPartners(), 'id', 'name')])->label('Партнёр') ?>
            <?php } else { ?>
                <?= $form->field($model, 'partner_id')->hiddenInput(['value' => $model->partner_id])->label(false) ?>
            <?php } ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'read_only')->checkbox(['label' => 'Только чтение'])?>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>


</div>
