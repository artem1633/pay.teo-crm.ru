<?php

use app\models\Partner;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'visible' => Users::isAdmin(),
        'label' => 'Статус',
        'format' => 'raw',
        'value' => function ($data) {
            $date_ban = \app\models\Tmp::find()->where(['user_id' => $data->id])->one()->date_end_user_ban;
            if ($data->login == 'admin') {
                return '';
            }
            if ($date_ban) {
                $class = 'btn fa fa-fw  fa-ban';
                $color = 'red';
                $title = 'Нажмите для снятия блокировки с пользователя';
                $action = 'unban';
            } else {
                $class = 'btn fa fa-fw  fa-check-circle';
                $color = 'green';
                $title = 'Нажмите для блокировки пользователя';
                $action = 'ban';
            }


            return Html::button('', [
                'style' => "color: {$color}; display:flex; justify-content: center;",
                'id' => $data->id,
                'title' => $title,
                'class' => $class,
                'onclick' => '$.post(
                                        "' . Url::toRoute('/tmp/switch-ban') . '", 
                                        {
                                            user_id: $(this).attr("id"),
                                            action: "' . $action . '",
                                        }, 
                                        function(res){
                                            //alert(res[0]);
                                            if (res[0] == "banned"){
                                                $("#" + res[1]).removeClass("fa-check-circle");
                                                $("#" + res[1]).addClass("fa-ban");
                                                $("#" + res[1]).attr("title", "Нажмите для снятия блокировки с пользователя");
                                                $("#" + res[1]).css({"color":"red"});
                                            } else if (res[0] == "unbanned"){
                                                $("#" + res[1]).removeClass("fa-ban");
                                                $("#" + res[1]).addClass("fa-check-circle");
                                                $("#" + res[1]).attr("title", "Нажмите для блокировки пользователя");
                                                $("#" + res[1]).css({"color":"green"});
                                            }
                                        }
                                        );'
            ]);
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'read_only',
        'label' => 'Режим',
        'value' => function ($data) {
            if ($data->read_only == 0) {
                return 'Без ограничений';
            } else {
                return 'Только чтение';
            }
        }

    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'surname',
        'label' => 'ФИО',

    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'login',
    ],

    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'password',
    ],*/
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'permission',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'balance',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'party_system_id',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'partner_id',
        'label' => 'Партнер',
        'value' => function ($data) {
            return Partner::getPartnerName($data->partner_id);
        },
        'visible' => Users::isAdmin(),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'visible' => Users::isAdmin(),
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
                return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'],
    ],

];