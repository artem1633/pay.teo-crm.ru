<?php

use app\models\Partner;
use app\models\Users;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'surname',
            'login',
            'phone',
            [
                'attribute' => 'partner_id',
                'value' => function($data){
                    return Partner::getPartnerName($data->partner_id);
                },
                'label' => 'Партнёр',
            ],
            [
                'attribute' => 'read_only',
                'value' => function($data){
                    if (Users::find()->where(['id' => $data->id])->one()->read_only){
                        return 'Только чтение';
                    }
                    return 'Без ограничений';
                },
                'label' => 'Партнёр',
            ],

        ],
    ]) ?>

</div>
