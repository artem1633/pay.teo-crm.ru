<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Log */
?>
<div class="log-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'log_time',
            'id',
            'partner_id',
            'user_id',
            'user_ip',
            'description',
            'result',
            'amount',
            'payee',
        ],
    ]) ?>

</div>
