<?php

namespace app\controllers;

use app\models\Driver;
use app\models\DriverSearch;
use app\models\Partner;
use app\models\Payment;
use app\models\Settings;
use app\models\TypePayment;
use app\models\Users;
use app\modules\api\controllers\ClientController;
use phpQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * DriverController implements the CRUD actions for Driver model.
 */
class DriverController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['offer', 'accept-offer'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Driver models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriverSearch();

        if (Users::isAdmin()) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        } else {
            $partner = Partner::getPartnerModel(Yii::$app->user->id);
            $dataProvider = new ActiveDataProvider([
                'query' => Driver::find()->where(['partner_id' => $partner->id])
            ]);
        }
        $dataProvider->setSort(['defaultOrder' => ['surname' => SORT_ASC]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Driver model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
//        $payment_model = Payment::find()->where(['driver_id' => $id])->all(); //Выплаты водителю
        $paymentDataProvider = new ActiveDataProvider([
            'query' => Payment::find()->where(['driver_id' => $id]),
        ]);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Driver #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                    'paymentDataProvider' => $paymentDataProvider,
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Driver model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Driver();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавление водителя",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Добавление водителя",
                    'content' => '<span class="text-success">Водитель создан успешно</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Добавить ещё', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Добавление водителя",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Driver model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update Driver #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Driver #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Update Driver #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Driver model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Driver model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Driver model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Driver the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driver::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Возвращает тип платежа и номер карты или киви
     *
     * @return string
     */
    public function actionInfoForPayment()
    {
//        return Json::encode(['QIWI', '12312312313']);

        $driver_id = \Yii::$app->request->post('driver_id');

        Yii::info('ID водителя: ' . $driver_id, __METHOD__);

        $driver = Driver::find()->where(['id' => $driver_id])->one();
        $number = 'Не указан';
        $type_payment = '';
        if (isset($driver)) {
            $type_payment = TypePayment::find()->where(['id' => $driver->type_payment_id])->one()->name;
            switch ($type_payment) {
                case 'QIWI':
                    $number = $driver->qiwi_number;
                    break;
                case 'Карта':
                    $number = $driver->card_number;
                    break;
            }

        }
        return Json::encode([$type_payment, $number]);
    }

    /**
     * Страница с текстом оферты
     *
     * @param int $id ID водителя
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionOffer($id)
    {
        $session = Yii::$app->session;

        //Парсим страницу с договором оферты
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('http://taxi1990.ru/oferta')
            ->send();
        if ($response->isOk) {
            $pq = phpQuery::newDocumentHTML($response->content);
            $in_body = $pq->find('.t-body');
            $text = $in_body->find('.t-text');
            $text->append(Html::a('Принять условия', Url::to(['driver/accept-offer', 'id' => $id]), [
                'class' => 'btn btn-primary btn-lg btn-block',
                'style' => 'color: white',
            ]));
            $session->set('body', $in_body->html());
//            VarDumper::dump($body, 10, true);

            return $this->render('offer', ['success' => true]);
        } else {
            return $this->render('offer', ['success' => false]);
        }
    }

    /**
     * Сохранение ссылки на оферту и сохранение согласия с условиями оферты
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAcceptOffer($id)
    {
        $driver_id = $id;

        Yii::info('ID водителя: ' . $driver_id, __METHOD__);

        $driver_model = $this->findModel($driver_id);

        $driver_model->link_offer = Url::toRoute(['driver/offer', 'id' => $driver_model->id]);

        $driver_model->accept_offer = 1;

        if ($driver_model->save()) {
            return Html::tag('div', Html::tag('h2', 'Условия приняты'), [
                'style' => 'margin-top: 10%; width: 100%; display: flex; justify-content: center;'
            ]);
        } else {
            return 'Невозможно принять условия оферты';
        }

    }

    public function actionGetDrivers()
    {
        $params[] = '';

        $result = ClientController::requestAPI('getDriversList', $params, 'GET');

        VarDumper::dump($result, 10, true);
    }

    /**
     * Добавление баланса партнёру пользователя
     *
     * @param double $amount сумма в рублях
     */
    public function actionAddBalance($amount)
    {
//        $amount = 30; //сумма в рублях
        $partner_model = Partner::find()->where(['id' => Partner::getPartnerId(Yii::$app->user->id)])->one();
        $params['id_driver'] = $partner_model->outer_city_id;
        $params['type'] = 'plus';
        $params['price'] = $amount;
        $params['description'] = 'Тестовое зачисление из PayPartner';

        $result_add_balance = ClientController::requestAPI('changeDriverBalance', $params);

        VarDumper::dump($result_add_balance, 10, true);
    }

    public function actionSendSmsToDriver($id)
    {

        $driver_phone = Driver::getDriverPhone($id);
        $text = Settings::getSmsText($id);

        ClientController::requestSMS($driver_phone, $text);

        return $this->actionIndex();
    }
}
