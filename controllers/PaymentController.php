<?php

namespace app\controllers;

use app\models\Driver;
use app\models\ImportError;
use app\models\Payment;
use app\models\PaymentSearch;
use app\models\TypePayment;
use app\models\UploadForm;
use app\models\Users;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentSearch();
        if (Users::isAdmin()) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Payment::find()->where(['owner_payment_id' => Yii::$app->user->id]),
            ]);
        }

        return $this->render('index', [
//            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Payment #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Payment model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Payment();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создание выплаты",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                //Переводим сумму на Qiwi

//                $transaction_result = Payment::createBill($model->qiwi_number, $model->amount, 'test');
//                $model->status = '1';
//                $model->save();

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Create new Payment",
                    'content' => '<span class="text-success">Платеж создан успешно</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать ещё', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Create new Payment",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Payment model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($model->status) {
            return 'Сейчас кто-то выхватит бан!';
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update Payment #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Payment #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Update Payment #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Payment model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($model->status) {
            return 'Сейчас кто-то выхватит бан!';
        }

        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Payment model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * получение информации о балансе аккаунта QIWI
     */
    public function actionGetQiwiBalance()
    {
        $balance = Payment::getQiwiBalance();

//        foreach ($res['accounts'] as $account) {
//            if ($account['hasBalance']) {
//                return $account['balance']['amount'];
//            }
//        }
        return $balance;
    }

    /**
     * Получение информации о балансе аккаунта Ситимобил
     */
    public function actionGetCityBalance()
    {
        $id = '2674xa8f4a70444eaa606be7dc64ad27074ab';
        $res = Payment::getCityBalance($id);

        VarDumper::dump($res, 10, true);
    }

//    /**
//     *
//     * @return string
//     * @throws \PhpOffice\PhpSpreadsheet\Exception
//     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
//     */
//    public function actionImportPayments()
//    {
//        $temp = substr(md5(microtime() . rand(0, 9999)), 0, 20);
//
//        if (Yii::$app->request->post()) {
//            $path_file = 'uploads/' . $temp . '.' . $this->file->extension;
//            $this->file->saveAs($path_file);
//        } else {
//            return $this->render('import');
//        }
//
//        if ($path_file) {
//
//            $inputFileName = $path_file;
//            /** Load $inputFileName to a Spreadsheet Object  **/
//            $spreadsheet = IOFactory::load($inputFileName);
//
//            // объект Cells, имеющий доступ к содержимому ячеек
//            $cells = $spreadsheet->getActiveSheet()->getCellCollection();
//            $active_sheet = $spreadsheet->getActiveSheet();
//            if (!$cells) {
//                return 'Пусто';
//            }
//            for ($row = 2; $row <= $cells->getHighestRow(); $row++) {
//                $cols = [];
//                for ($col = 1; $col <= 3; $col++) {
//                    $cols[] = $active_sheet->getCellByColumnAndRow($col, $row)->getValue();
//                }
//                if (is_numeric($cols[0])) {
//                    $payment_model = new Payment();
//
//                    $payment_model->driver_id = $cols[0];
//                    $type_payment = $cols[1];
//
//                    switch ($type_payment) {
//                        case 'QIWI':
//                            $payment_model->qiwi_number = Driver::findOne($payment_model->driver_id)->qiwi_number;
//                            break;
//                        case 'Карта':
//                            $payment_model->card_number = Driver::findOne($payment_model->driver_id)->card_number;
//                            break;
//                    }
//
//                    $payment_model->amount = $cols[2];
//
//                    //Записываем в базу
//                    $payment_model->save();
//                }
//            }
//
//            if ($payment_model->hasErrors()) {
//                VarDumper::dump($payment_model->errors, 10, true);
//            } else {
//                return $this->actionIndex();
//            }
//        }
////        VarDumper::dump($tbl_row, 10, true);
////        return 'Конец';
//    }


    /**
     * @return array|mixed|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function actionUpload()
    {
        $model = new UploadForm();
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Импорт выплат из файла",
                'content' => $this->renderAjax('import', [
                    'model' => $model,
                ]),
//                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

            ];
        } elseif (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $tmp_name = time() + rand(999, 99999999);

            if ($model->upload($tmp_name)) {
//                VarDumper::dump($model, 10, true);
                // Файл успешно загружен
                $inputFileName = 'uploads/' . $tmp_name . '.' . $model->file->extension;

                /** Load $inputFileName to a Spreadsheet Object  **/
                $spreadsheet = IOFactory::load($inputFileName);

                // объект Cells, имеющий доступ к содержимому ячеек
                $cells = $spreadsheet->getActiveSheet()->getCellCollection();
                $active_sheet = $spreadsheet->getActiveSheet();

                for ($row = 2; $row <= $cells->getHighestRow(); $row++) {
                    $cols = [];
                    for ($col = 1; $col <= 3; $col++) {
                        $cols[] = $active_sheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

                    if (is_numeric($cols[0])) {
                        $payment_model = new Payment();

                        $payment_model->driver_id = $cols[0];
                        $imports_type_payment = $cols[1];

                        $payment_model->type_payment = TypePayment::find()->where(['name' => $imports_type_payment])->one()->id;

                        Yii::info('Тип выплаты: ' . $payment_model->type_payment, __METHOD__);

                        switch ($imports_type_payment) {
                            case 'Qiwi':
                                $payment_model->qiwi_number = Driver::findOne($payment_model->driver_id)->qiwi_number;
                                break;
                            case 'Карта':
                                $payment_model->card_number = Driver::findOne($payment_model->driver_id)->card_number;
                                break;
                        }

                        $payment_model->amount = $cols[2];

                        Yii::info('Тип выплаты при импорте: ' . $payment_model->type_payment, __METHOD__);

                        //Записываем в базу
                        if (!$payment_model->save()) {
                            //Пишем ошибку в лог
                            $log = new ImportError();
                            $log->driver_id = $payment_model->driver_id; //Получатель платежа
                            $log->user_id = Yii::$app->user->id;
                            $log->user_ip = Yii::$app->request->userIP;
                            $log->description = $payment_model->errors['error'][0];
                            $log->amount = $payment_model->amount;
                            $log->type_payment = $payment_model->type_payment;
                            if (!$log->save()){
                                Yii::error($log->errors, __METHOD__);
                            }
                        };
                    }
                }
                //Удаляем на сервере импортированный файл
                unlink('uploads/' . $tmp_name . '.' . $model->file->extension);
            }
            return $this->actionIndex();
        }

//        if (Yii::$app->request->isPost) {
//            $model->file = UploadedFile::getInstance($model, 'file');
//            $tmp_name = time() + rand(999, 99999999);
//
//            if ($model->upload($tmp_name)) {
////                VarDumper::dump($model, 10, true);
//                // file is uploaded successfully
//                $inputFileName = 'uploads/' . $tmp_name . '.' . $model->file->extension;
//                /** Load $inputFileName to a Spreadsheet Object  **/
//                $spreadsheet = IOFactory::load($inputFileName);
//
//                // объект Cells, имеющий доступ к содержимому ячеек
//                $cells = $spreadsheet->getActiveSheet()->getCellCollection();
//                $active_sheet = $spreadsheet->getActiveSheet();
//                if (!$cells) {
//                    return 'Пусто';
//                }
//
//                for ($row = 2; $row <= $cells->getHighestRow(); $row++) {
//                    $cols = [];
//                    for ($col = 1; $col <= 3; $col++) {
//                        $cols[] = $active_sheet->getCellByColumnAndRow($col, $row)->getValue();
//                    }
//
//                    if (is_numeric($cols[0])) {
//                        $payment_model = new Payment();
//
//                        $payment_model->driver_id = $cols[0];
//                        $type_payment = $cols[1];
////                        VarDumper::dump($type_payment);
//
//                        switch ($type_payment) {
//                            case 'QIWI':
//                                $payment_model->qiwi_number = Driver::findOne($payment_model->driver_id)->qiwi_number;
//                                break;
//                            case 'Карта':
//                                $payment_model->card_number = Driver::findOne($payment_model->driver_id)->card_number;
//                                break;
//                        }
//
//                        $payment_model->amount = $cols[2];
//
////                        VarDumper::dump($payment_model, 10, true);
//
//                        //Записываем в базу
//                        if (!$payment_model->save()) {
//                            VarDumper::dump($payment_model->errors, 10, true);
//                        };
//                    }
//                }
//                //Удаляем на сервере импортированный файл
//                unlink('uploads/' . $tmp_name . '.' . $model->file->extension);
//            }
//            return $this->actionIndex();
//        }

//        return $this->render('upload', ['model' => $model]);
        return $this->render('import', ['model' => $model]);
    }


}
