<?php

namespace app\controllers;

use app\models\Settings;
use app\models\Tmp;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get', 'post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionAvtorizatsiya()
    {
        if (isset(Yii::$app->user->identity->id)) {
            return $this->render('error');
        } else {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return void
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    public function actionInstruksiya()
    {
        return $this->render('instruksiya');
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if ($session['menu'] == null | $session['menu'] == 'large') $session['menu'] = 'small';
        else $session['menu'] = 'large';

        return $session['menu'];
    }

    /**
     * @return bool|string|Response
     */
    public function actionValidateUser()
    {
        $request = Yii::$app->request;

        $username = $request->post('username');
        $password = $request->post('password');


        $user = Users::find()->where(['login' => $username])->one();

        if ($user->permission == 'administrator') {
            $is_admin = true;
        } else {
            $is_admin = false;
        }

        if (!$user || $user->password != md5($password)) {
            return 'Неверный логин или пароль';
        }
        //Вход админом без смс подтвержддения
        if ($is_admin) {
            $model = new LoginForm();
            $model->username = $username;
            $model->password = $password;
            $model->login();
            return $this->goBack();
        }

        //Если не админ и нет телефона
        if (!$user->phone && !$is_admin) {
            return 'Отсутствует номер телефона. Вход невозможен.';
        }

        if (!Settings::getSettingValueByKey('sms_gate_login') || !Settings::getSettingValueByKey('sms_gate_password')) {
            return 'Отсутствуют настройки СМС шлюза, вход невозможен.';
        }


        //Проверяем запись в таблице tmp
        $tmp_model = Tmp::find()->where(['user_id' => $user->id])->one();

        if (!$tmp_model->id) { //Если модель пустая
            $tmp_model = new Tmp();
            $tmp_model->code = rand(10000, 99999);
            $tmp_model->user_id = $user->id;
            $tmp_model->num_wrong_code = 0;
            $tmp_model->save();
        }

        //проверяем дату бана
        Yii::info(strtotime($tmp_model->date_end_user_ban) > time(), __METHOD__);

        if ($tmp_model->date_end_user_ban && strtotime($tmp_model->date_end_user_ban) > time()) {
            return 'Вход невозможен. Попробуйте позже';
        } else {
            $tmp_model->date_end_user_ban = null;
            $tmp_model->num_wrong_code = 0;
            $tmp_model->save();
        }

        //Отправляем смс код авторизации

        $phone = preg_replace('/[^0-9]/', '', $user->phone);

        $phone = mb_substr($phone, strlen($phone) - 10);
//        $result = 'Accepted;';
        $result = $this->requestSMS('+7' . $phone, ' Код авторизации "ВП": ' . $tmp_model->code);

        if (mb_strpos($result, 'cepted;')) {
            return 'ok';
        } else {
            return Json::encode($result);
        }
    }

    /**
     * @param $phone_number
     * @param $message
     * @return bool|string
     */
    protected function requestSMS($phone_number, $message)
    {
        $url = 'http://api.iqsms.ru/messages/v2/send/';

        $params['login'] = Settings::getSettingValueByKey('sms_gate_login');
        $params['password'] = Settings::getSettingValueByKey('sms_gate_password');
        $params['phone'] = $phone_number;
        $params['text'] = $message;
        $params['flash'] = 1;

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result;

    }

    /**
     * Валидация кода подтверждения авторизации
     *
     * @return string|Response
     */
    public function actionValidateCode()
    {

        $request = Yii::$app->request;
        $code = $request->post('code');
        $username = $request->post('username');
        $password = $request->post('password');

        $user = Users::find()->where(['login' => $username])->one();

        $tmp_model = Tmp::find()->where(['user_id' => $user->id])->one();

        //Проверяем кол-во попыток неверного ввода смс кода
        if ($tmp_model->num_wrong_code >= Settings::getSettingValueByKey('num_wrong_code')) {
            $ban_date = date('Y-m-d H:i:s', strtotime('+10 minutes', time()));
            Yii::info($ban_date, __METHOD__);
            $tmp_model->date_end_user_ban = $ban_date;
            $tmp_model->save();
            $this->goHome();
        }

        $data1 = strtotime($tmp_model->code_create_at);
        $data2 = time();
        $interval = $data2 - $data1;

        Yii::info($data1, __METHOD__);
        Yii::info($data2, __METHOD__);
        Yii::info($interval, __METHOD__);

        if ($interval > 60) {  //если код просрочен (действие кода 60 секунд)
            Tmp::deleteAll('user_id=:user_id', [':user_id' => $user->id]);
            return $this->goHome();
        } else {
            //Если код верен
            if ($tmp_model->code == $code) {
                $model = new LoginForm();
                $model->username = $username;
                $model->password = $password;
                $model->login();
                Tmp::deleteAll('user_id=:user_id', [':user_id' => $user->id]);
                return $this->goBack();
            } else {
                Yii::info($tmp_model->num_wrong_code, __METHOD__);
                $tmp_model->num_wrong_code = $tmp_model->num_wrong_code + 1;
                Yii::info($tmp_model->num_wrong_code, __METHOD__);
                $tmp_model->save();
                Yii::info($tmp_model->num_wrong_code, __METHOD__);

                return 'Неверный СМС код подтверждения';
            }
        }
    }


}
